
#   Entwicklung einer spieltheoretischen Simulation des Kartenspiels Bettlern (ugs. "Arscchloch") zu Strategieanalyse 

- Programmiersprache: Java

- Seminararbeit in der Oberstufe
- Reine Backend-App zur statistischen Auswertung von Millionen Spielen von „Strategie-Bots“

# Ausführung der Simulation

- optimalerweise JDK 19 
- Mainklasse: in Main-Methode kann Simulations Objekt n Spiele simulaieren
- Methode auswählen zur Anzahl der Wiederholungen
- Strategien auswählen mit int werten (Strategien 1-8)

# Legende zu Ausgaben in Konsole:

- Spielkarte X Y
<br>
X: Farbe: 0 = Blatt , 1 = Pik , 2 = Caro , 3 = Herz
<br>
Y: Zahl: 0 = 6 , 1 = 7 , 2 = 8 , 3 = 9 ; 4 = 10 
    //*5 = Bube , 6 = Dame , 7 = König , 8 = Ass
    <br>
    <br>
    Beispiel: Spielkarte 3 8 => Herz Ass
