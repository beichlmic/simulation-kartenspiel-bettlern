public class Mitte {
//Kartenstapel auf den man trumpft
    
    int mehrlinge; //Mehrlinge in der Mitte
    int zahl; //Zahl der Karte(n) in der Mitte

    
    public Mitte() {
       mehrlinge = 0;
       zahl = -1;
    }
    
    public void setzeZahl(int z) {
        zahl = z;
    }
    public void setzeMehrlinge(int m) {
        mehrlinge = m;
    }
    public void setzeMitte(int m, int z) {
        
        mehrlinge = m;
        zahl = z;
    }
    public int gebeZahl() {
        return zahl;
    }
    public int gebeMehrlinge() {
        return mehrlinge;
    }
   
    public void MitteOutprint() {
        System.out.println("Mitte Mehrlingsanzahl: " + gebeMehrlinge() + " Zahl: " + gebeZahl());
        
    }
    
    
}