public class Spieler {//extends SpielSpieler{

    String Username;
    Listenelement kleinstesBlatt;  // = anfang von liste
    //Spielkarte [] spielkarte;
    boolean fertig = false;
    int schonFertig = 0; //"schonFertig == 1" bedeutet, dass der Spieler in der letzen Runde feritg gemacht hat bzw. zum ersten Mal fertig gemacht hat
    int zahlGespielt = -1;
    int mehrlingsSpeicher = -1; //speichert wie viele mehrlinge ein Spieler gelegt hat
    int zahlSpeicher = -1; //speichert welche zahl ein Spieler gelegt hat
    boolean ausgesetzt = false;
    boolean [] verteilt;
    int bombe = -1; //gibt an, ob und auf auf welcher Zahl ein Spieler eine bombe hat
    Listenelement s = null; 
    Listenelement mehrlingsSplit = null; //speichert 2er-Mehrlinge zwischen um sie eventuell aufzuteilen und einen zu legen, falls es die Situation erfordert

    public Spieler (String NewUsername) {
        Username = NewUsername;
        kleinstesBlatt = new Abschluss();
        //spielkarte = new Spielkarte [36];
        //Spieler spieler1 = new Spieler();
        //Spieler spieler2 = new Spieler();
        //Spieler spieler3 = new Spieler();
        verteilt = new boolean [36];

        //new Spielkarte(zahl, farbe);
    }

    public String gebeSpieler() {
        return Username;
    }

    public void setzeKleinstesBlatt(Listenelement k) {
        kleinstesBlatt = k;
    }

    public void setzeSchonFertig(int sf) {
        schonFertig = sf;
    }

    public int gebeSchonFertig() {
        return schonFertig;
    }

    public void setzeFertig(boolean f) {
        fertig = f;
    }

    public boolean gebeFertig() { 
        //gibt zurück, ob ein Spieler keine fertig ist, also keine Karten mehr hat
        if(kleinstesBlatt.IstAbschluss()) {
            fertig = true;
            return fertig;
        }
        else {
            fertig = false;
            return fertig;
        }
    }

    public int gebeTemp() {
        zahlGespielt = kleinstesBlatt.gebeTemp();
        return zahlGespielt;
    }

    public void setzeAusgesetzt(boolean b) {
        ausgesetzt = b;
    }

    public boolean gebeAusgesetzt() {
        return ausgesetzt;
        /*ausgesetzt = kleinstesBlatt.gebeAusgesetzt();
        return ausgesetzt;*/
    }

    public int gebeMehrlingsSpeicher() { 

        return mehrlingsSpeicher;
    }

    public int gebeZahlSpeicher() {
        return zahlSpeicher;
    }

    public boolean gebeTrumpf() {
        return kleinstesBlatt.gebeTrumpf();
    }

    public void setzeTrumpf(boolean t){
        kleinstesBlatt.setzeTrumpf(t);
    }

    public Listenelement gebeKleinstesBlatt() {
        return kleinstesBlatt;
    }

    public void setzeBombe(int b) {
        bombe = b;
    }

    public Listenelement BombeNichtAusspielen() {
        return kleinstesBlatt.BombeNichtAusspielen();
    }

    public void verteiltArrayDeklarieren() {
        for(int i = 0; i <= 35; i++) {
            verteilt[i] = false;

        }
    }

    public boolean AllesVerteilt(int v) {
        if(v == 35) {
            return true;
        }
        else{
            if(verteilt[v] == true) {
                AllesVerteilt(v + 1);
                return false;
            }
            else{
                return false;

            }
        }
    }

    public void Random35() {
        int x = (int) ((Math.random()) * 36);
        System.out.println("rdm: " + x);

    }

    public void SpielkarteErstellen() {
        int x = (int) ((Math.random()) * 36);
        //int rdm = (int) (math() *36);
        //Spielkarte k = spielkarte [(int) (math.random()) *36] ;
        /*
        if(spielkarte[x] != null) {
        Spielkarte k = spielkarte[x];
        spielkarte[x] = null;
        System.out.println("Spielkarte mit Zahl " + k.gebeZahl() + " und Farbe " + k.gebeFarbe() + " wurde erstellt und ist nicht mehr im Deck vorhanden");
        System.out.println("Spielkarte " + k.gebeZahl() +" "+ k.gebeFarbe() + " ist im Besitz von " + gebeSpieler());
        }
        else{
        SpielkarteErstellen();
        }
         */
    }

    public void SpielkarteErstellenHinzufuegen(Spielkarte k) { 
        //neue Spielkarte wird erstellt und hinzugefügt
        //Überprüfung, ob Hand des Spielers leer ist
        if(kleinstesBlatt.IstAbschluss())
        {
            kleinstesBlatt = k;
            k.setzeNachfolger(new Abschluss());
        }    
        else {
            boolean temp = kleinstesBlatt.SpielkarteHinzufuegen(k);
            if(temp == true) {
                kleinstesBlatt = k;
            }
        }
        SpielkarteReturn(k);

    }

    public Spielkarte SpielkarteReturn(Spielkarte s) {
        return s;
    }

    public void HandPrint() {
        System.out.println("Hand von " + gebeSpieler() + ":");
        //System.out.println("           F Z ");
        kleinstesBlatt.HandPrintExtended();

    }

    //public Spielkarte kErstellenReturnen() {
    //    return k;
    //}

    public void Austeilen2 () {
    }

    public int gebeAnzahlKarten () {
        return 1 + kleinstesBlatt.gebeAnzahlKarten();
    }

    public Listenelement entferne(int farbe, int  zahl) {
        //Spielekarte mit bestimmter Farbe und Zahl wird entfernt
        //rekursiver Aufruf an kleinstesBlatt, gibt das entfernte Listenelement zurück 
        if(kleinstesBlatt.gebeFarbe() == farbe && kleinstesBlatt.gebeZahl() == zahl) {
            kleinstesBlatt = kleinstesBlatt.gebeNachfolger();
        }
        System.out.println("Karten der Spieler:");
        System.out.println("");

        return kleinstesBlatt.entferne(farbe, zahl);
    }

    public Listenelement entferne2(Listenelement spielkarte ) {
        //ein bestimmtes Listenelement wird entfernt und zurückgegeben
        //rekursiver Aufruf an kleinstesBlatt

        if(spielkarte.IstAbschluss()) {
            //System.out.println("                    debug_                    debug_Abschluss sollte entferntwerden: spielkarte.IstAbschluss() == true ");
            return null;
        }
        else{

            if(kleinstesBlatt.gebeFarbe() == spielkarte.gebeFarbe() && kleinstesBlatt.gebeZahl() == spielkarte.gebeZahl()) {

                //}
                //Listenelement nn =  kleinstesBlatt.gebeNachfolger().gebeNachfolger();
                kleinstesBlatt = kleinstesBlatt.gebeNachfolger();
                //kleinstesBlatt.setzeNachfolger(nn);
            }
            //mitte.setzeMitte(kleinstesBlatt.MehrlingCheck(zahl),zahl) ;
            System.out.println("Karten der Spieler:");
            System.out.println("");

            /*mitte.setzeMehrlinge(1);
            mitte.setzeZahl(zahl);
            mitte.MitteOutprint();
             */
            return kleinstesBlatt.entferne(spielkarte.gebeFarbe(), spielkarte.gebeZahl());
        }
    }

    public int AkutelleZahlEntfernen() {
        //entfernt alle Karten mit der Zahl vonn kleinstesBlatt
        kleinstesBlatt.AktuelleZahlEntfernen();

        return kleinstesBlatt.AktuelleZahlEntfernen();
    }

    public Listenelement WipeOutNumber(int v) {
        //entfernt alle Zahlen mit einer bestimmten Zahl
        return     kleinstesBlatt.WipeOutNumber(v);
    }

   

    public Listenelement Legen4(int mehrlingeMitte, int zahlMitte) {
//siehe in Seminararbeit Kapitel 5.3.1
        

        if(s == null) {
            
            s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte );
        }

        if(s != null) {
            //System.out.println("                    debug_                    debug_s nicht Null");
            if(s.gebeZahl() > zahlMitte ) {
                if(MehrlingsCheck2(s.gebeZahl()) == mehrlingeMitte) {

                    //System.out.println("                    debug_                    debug_IIa");
                    switch(MehrlingsCheck2(s.gebeZahl())) {

                        case 4:
                        //System.out.println("                    debug_                    debug_IIId");
                        if(zahlMitte == 8) {
                            ausgesetzt = false;
                            mehrlingsSpeicher = 4;
                            System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" +  s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl());
                            entferne2(s.gebeNachfolger().gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger());
                            entferne2(s);

                        }
                        else {
                            bombe = s.gebeZahl();

                            if(zahlMitte + 4 >= s.gebeNachfolger().gebeZahl()) {
                                if(s.gebeNachfolger().IstAbschluss() == false) {
                                    s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                                    /*Listenelement t = */Legen4(mehrlingeMitte, zahlMitte) ;
                                    // s = t;
                                }
                                else{
                                    if(EndGameCase()) {
                                        Legen4EndGameCase(mehrlingeMitte, zahlMitte);
                                    }
                                    else{
                                        ausgesetzt = true;
                                        mehrlingsSplit = null;
                                        s = null;
                                    }

                                }

                            }
                            else {
                                ausgesetzt = true;
                                s = null;

                            }
                        }

                        break;
                        case 3:
                        //System.out.println("                    debug_                    debug_IIIc");
                        mehrlingsSpeicher = 3;
                        ausgesetzt = false;

                        System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));  // hier Null Pointer exeption: problem: ignoreit bei 3er-Mehrlingen den ersten der drei und fängt beim zweite nan

                        entferne2(s.gebeNachfolger().gebeNachfolger());
                        entferne2(s.gebeNachfolger());
                        entferne2(s);
                        break;
                        case 2:;
                        //System.out.println("                    debug_                    debug_IIIb");
                        ausgesetzt = false;
                        mehrlingsSpeicher = 2;
                        System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));

                        entferne2(s.gebeNachfolger());
                        entferne2(s);
                        break;
                        case 1: 
                        //System.out.println("                    debug_                    debug_IIIa");
                        ausgesetzt = false;
                        mehrlingsSpeicher = 1;
                        System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));

                        entferne2(s);
                        break;
                    }
                }
                else {
                    if(MehrlingsCheck2(8) > 1 && mehrlingeMitte == 1) {
                        s = entferneErstesAss();

                    }
                    else{
                        if(s.gebeZahl() == 8 && mehrlingeMitte == 1) {
                            System.out.println(gebeSpieler() + " legt " + s.gebeFarbe() + "_" + s.gebeZahl());
                            entferne2(s);
                        }else{
                            //System.out.println("                    debug_                    debug_IIb");
                            //if(s != null) {
                            if(MehrlingsCheck2(s.gebeZahl()) == 2 && mehrlingsSplit == null ) {
                                mehrlingsSplit = s;
                                //System.out.println("                    debug_                    debug_s ist " + s.gebeFarbe() + "_" + s.gebeZahl());

                                //}      
                            }
                            /*else {
                            s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte );
                            if(MehrlingsCheck2(s.gebeNachfolger().gebeZahl()) == 2 && mehrlingsSplit == null ) {
                            mehrlingsSplit = s.gebeNachfolger();

                            }
                            }*/

                            if(zahlMitte + 4 >= s.gebeNachfolger().gebeZahl()) {
                                //System.out.println("                    debug_                    debug_IIba");
                                //kleinstesBlatt.setzeS (s);
                                if(s.gebeNachfolger().IstAbschluss() == false) {
                                    s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                                    /*Listenelement t = */Legen4(mehrlingeMitte, zahlMitte) ;
                                    // s = t;
                                }
                                else{
                                    ausgesetzt = true;
                                    mehrlingsSplit = null;
                                    s = null;

                                }
                            }
                            else {
                                //System.out.println("                    debug_                    debug_IIbaIV");
                                if(mehrlingsSplit != null) {
                                    if(mehrlingeMitte == 1) {
                                        //System.out.println("                    debug_                    debug_IIbaIVa");
                                        //System.out.println("                    debug_                    debug_s " + s.gebeFarbe() + "_" + s.gebeZahl() + " wird nun aufgerufen und verglichen");
                                        if(MehrlingsCheck2(mehrlingsSplit.gebeZahl()) == 2/*s.gebeNachfolger().Legen4(mehrlingeMitte, zahlMitte).gebeZahl()) == 2*/) {
                                            //System.out.println("                    debug_                    debug_IIbaIVaa");
                                            System.out.println(gebeSpieler() + " legt: " + mehrlingsSplit.gebeFarbe() + "_" + ( mehrlingsSplit.gebeZahl()));
                                            entferne2(mehrlingsSplit);
                                            s = mehrlingsSplit;
                                            mehrlingsSplit = null;
                                            mehrlingsSpeicher = 1;
                                            ausgesetzt = false;

                                        }
                                        else{
                                            //System.out.println("                    debug_                    debug_IIbaIVab");
                                            if(mehrlingsSplit != null && mehrlingsSplit.gebeNachfolger() != null)
                                                if(zahlMitte + 2 >= mehrlingsSplit.gebeNachfolger().gebeZahl()) {
                                                    //System.out.println("                    debug_                    debug_IIbaIVaba");
                                                    mehrlingsSplit = mehrlingsSplit.gebeNachfolger();
                                                    Listenelement t = Legen4(mehrlingeMitte, zahlMitte) ;
                                                    mehrlingsSplit = t;
                                                }
                                                else{
                                                    //System.out.println("                    debug_                    debug_IIbaIVabb");
                                                    ausgesetzt = true;
                                                    mehrlingsSplit = null;
                                                    s = null;

                                                }
                                            else {
                                                //System.out.println("                    debug_                    debug_IIbaIVabb");
                                                ausgesetzt = true;
                                                mehrlingsSplit = null;
                                                s = null;
                                            }
                                        }

                                    }
                                    else{
                                        if(EndGameCase()) {
                                            Legen4EndGameCase(mehrlingeMitte, zahlMitte);
                                        }
                                        else{
                                            //System.out.println("                    debug_                    debug_IIbaIVb");
                                            ausgesetzt = true;
                                            mehrlingsSplit = null;
                                            s = null;

                                        }
                                    }
                                }
                                else{
                                    //System.out.println("                    debug_                    debug_IIbaIVb");
                                    ausgesetzt = true;
                                    mehrlingsSplit = null;
                                    s = null;

                                }
                            }
                        }
                    }
                }
            }
            else {
                //System.out.println("                    debug_                    debug_s < zahlMitte => wird klBl.Legen4()");
                if(s.gebeNachfolger().IstAbschluss() == false) {
                    s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                    /*Listenelement t = */Legen4(mehrlingeMitte, zahlMitte) ;
                    // s = t;
                }
                else{
                    if(EndGameCase()) {
                        Legen4EndGameCase(mehrlingeMitte, zahlMitte);
                    }
                    else{
                        ausgesetzt = true;
                        mehrlingsSplit = null;
                        s = null;
                    }
                }

            }

            if(s != null) {
                zahlSpeicher = s.gebeZahl();
            }
            if(s == null) {
                ausgesetzt = true;
            }

        }
        //System.out.println("                    debug_                    debug_s retunt");
        return s;
    }

    public Listenelement Legen4Passiv(int mehrlingeMitte, int zahlMitte) {
        //siehe Seminararbeit Kapitel 5.3.1 mit einzige Änderung, das nur zurückgegeben wird, aber nichts entfernt wird
        if(s == null) {
            //System.out.println("                    debug_s war Null wird klBl.Legen4()");
            s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte );
        }
        if(s != null) {
            // System.out.println("s nicht Null");
            if(s.gebeZahl() > zahlMitte ) {
                if(MehrlingsCheck2(s.gebeZahl()) == mehrlingeMitte) {

                    //System.out.println("                    debug_IIa");
                    switch(MehrlingsCheck2(s.gebeZahl())) {

                        case 4:
                        //System.out.println("                    debug_IIId");
                        if(zahlMitte == 8) {
                            ausgesetzt = false;
                            mehrlingsSpeicher = 4;
                            /*System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" +  s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl());
                            entferne2(s.gebeNachfolger().gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger());
                            entferne2(s);*/

                        }
                        else {
                            bombe = s.gebeZahl();

                            if(zahlMitte + 4 >= s.gebeNachfolger().gebeZahl()) {
                                if(s.gebeNachfolger().IstAbschluss() == false) {
                                    s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                                    /*Listenelement t = */Legen4Passiv(mehrlingeMitte, zahlMitte) ;
                                    // s = t;
                                }
                                else{
                                    ausgesetzt = true;
                                    mehrlingsSplit = null;
                                    s = null;

                                }
                            }
                            else {
                                if(EndGameCase()) {
                                    Legen4EndGameCase(mehrlingeMitte, zahlMitte);
                                }
                                else{
                                    ausgesetzt = true;
                                    mehrlingsSplit = null;
                                    s = null;
                                }
                            }
                        }

                        break;
                        case 3:
                        //System.out.println("                    debug_IIIc");
                        mehrlingsSpeicher = 3;
                        ausgesetzt = false;
                        //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                        /*System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));

                        entferne2(s.gebeNachfolger().gebeNachfolger());
                        entferne2(s.gebeNachfolger());
                        entferne2(s);*/
                        break;
                        case 2:;
                        /*System.out.println("IIIb");
                        ausgesetzt = false;
                        mehrlingsSpeicher = 2;
                        System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));

                        //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                        entferne2(s.gebeNachfolger());
                        entferne2(s);*/
                        break;
                        case 1: 
                        // System.out.println("IIIa");
                        ausgesetzt = false;
                        mehrlingsSpeicher = 1;
                        /* System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                        entferne2(s);*/
                        break;
                    }
                }
                else {     
                    // System.out.println("IIb");

                    if(MehrlingsCheck2(s.gebeZahl()) == 2 && mehrlingsSplit == null ) {

                        mehrlingsSplit = s;
                        //System.out.println("                    debug_mehrlingsSplit wurde von auf " + mehrlingsSplit.gebeFarbe() + "_" + mehrlingsSplit.gebeZahl() +" gesetzt");

                    }      

                    if(zahlMitte + 4 >= s.gebeNachfolger().gebeZahl()) {
                        //System.out.println("                    debug_IIba");
                        //kleinstesBlatt.setzeS (s);
                        if(s.gebeNachfolger().IstAbschluss() == false) {
                            s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                            /*Listenelement t = */Legen4Passiv(mehrlingeMitte, zahlMitte) ;
                            // s = t;
                        }
                        else{

                        }
                    }
                    else {
                        // System.out.println("IIbaIV");
                        if(mehrlingeMitte == 1 && mehrlingsSplit != null) {
                            // System.out.println("IIbaIVa");
                            //System.out.println("                    debug_mehrlingsSplit " + mehrlingsSplit.gebeFarbe() + "_" + mehrlingsSplit.gebeZahl() +" wird nun aufgerufen und überprüft");
                            if(MehrlingsCheck2(mehrlingsSplit.gebeZahl()) == 2/*s.gebeNachfolger().Legen4(mehrlingeMitte, zahlMitte).gebeZahl()) == 2*/) {
                                //System.out.println("                    debug_IIbaIVaa");
                                /*System.out.println(gebeSpieler() + " legt: " + mehrlingsSplit.gebeFarbe() + "_" + ( mehrlingsSplit.gebeZahl()));
                                entferne2(mehrlingsSplit);*/
                                s = mehrlingsSplit;
                                mehrlingsSplit = null;
                                mehrlingsSpeicher = 1;

                            }
                            else{
                                //System.out.println("                    debug_IIbaIVab");
                                //System.out.println("                    debug_mehrlingsSplit " + mehrlingsSplit.gebeFarbe() + "_" + mehrlingsSplit.gebeZahl() +" wird nun aufgerufen und überprüft");
                                if(mehrlingsSplit.IstAbschluss() == false && mehrlingsSplit.gebeNachfolger().IstAbschluss() == false) {
                                    if(zahlMitte + 2 >= mehrlingsSplit.gebeNachfolger().gebeZahl()) {
                                        // System.out.println("IIbaIVaba");
                                        mehrlingsSplit = mehrlingsSplit.gebeNachfolger();
                                        Listenelement t = Legen4Passiv(mehrlingeMitte, zahlMitte) ;
                                        mehrlingsSplit = t;
                                    }
                                    else{
                                        //System.out.println("                    debug_IIbaIVabb");
                                        ausgesetzt = true;
                                        mehrlingsSplit = null;
                                        s = null;

                                    }
                                }
                            }

                        }
                        else{//System.out.println("                    debug_IIbaIVb");
                            ausgesetzt = true;
                            mehrlingsSplit = null;
                            s = null;
                        }
                    }
                }
            }
            else {
                //System.out.println("                    debug_                    debug_s < zahlMitte => wird klBl.Legen4Passiv()");
                if(s.gebeNachfolger().IstAbschluss() == false) {
                    s = s.gebeNachfolger();//.Legen4(mehrlingeMitte, zahlMitte);
                    /*Listenelement t = */Legen4Passiv(mehrlingeMitte, zahlMitte) ;
                    // s = t;
                }
                else{
                    ausgesetzt = true;
                    mehrlingsSplit = null;
                    s = null;

                }

            }

        }
        if(s != null) {
            zahlSpeicher = s.gebeZahl();
        }
        if(s == null) {
            ausgesetzt = true;
        }
        //System.out.println("                    debug_                    debug_s retunt");
        return s;
    }

    public Listenelement Legen4EndGameCase(int mehrlingeMitte, int zahlMitte) {
        //spezielle Version für Legen für den Fall, dass ein Spieler nur noch sehr wenige Karten hat
        if(s == null) {
            //System.out.println("                    debug_                    debug_s war Null wird klBl.Legen4()");
            s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte );
        }

        if(s != null) {
            if(s.gebeZahl() > zahlMitte ) {
                if(mehrlingeMitte == 1) {
                    System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                    entferne2(s);
                }
                else {
                    if(MehrlingsCheck2(s.gebeZahl()) == 3 && mehrlingeMitte == 2) {
                        System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                        entferne2(s.gebeNachfolger());
                        entferne2(s);
                    }
                    else {
                        if(MehrlingsCheck2(s.gebeZahl()) == 4) {
                            System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" +  s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl());
                            entferne2(s.gebeNachfolger().gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger());
                            entferne2(s); 
                        }
                    }

                }
            }
            else{
                s = null;
                ausgesetzt = true;
            }
        }
        return s;
    }

    public Listenelement Legen4EndGameCasePassive(int mehrlingeMitte, int zahlMitte) {
        //spezielle Version für Legen für den Fall, dass ein Spieler nur noch sehr wenige Karten hat, aber ohne entfernen nur mit Rückgabewert
        if(s == null) {
            //System.out.println("                    debug_s war Null wird klBl.Legen4()");
            s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte );
        }

        if(s != null) {
            if(s.gebeZahl() > zahlMitte ) {
                if(mehrlingeMitte == 1) {
                    //System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                    //entferne2(s);
                }
                else {
                    if(MehrlingsCheck2(s.gebeZahl()) == 3 && mehrlingeMitte == 2) {
                        //System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                        //System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                        //entferne2(s.gebeNachfolger());
                        //entferne2(s);
                    }
                    else {
                        if(MehrlingsCheck2(s.gebeZahl()) == 4) {
                            /*System.out.println(gebeSpieler() + " legt: " + s.gebeFarbe() + "_" + ( s.gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( s.gebeNachfolger().gebeNachfolger().gebeZahl()));
                            System.out.println(gebeSpieler() + " legt: " + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" +  s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl());
                            entferne2(s.gebeNachfolger().gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger().gebeNachfolger());
                            entferne2(s.gebeNachfolger());
                            entferne2(s); */
                        }
                    }

                }
            }
            else{
                s = null;
                ausgesetzt = true;
            }
        }
        return s;
    }

    public boolean EndGameCase() {
        //Legt fest, was "wenige Karten" bedeutet"
        if(gebeAnzahlKarten() == 2 || gebeAnzahlKarten() == 3) {
            if(MehrlingsCheck2(kleinstesBlatt.gebeZahl()) == 2 ||  MehrlingsCheck2(kleinstesBlatt.gebeZahl()) == 3 ) {
                return true;
            }
            else { return false;
            }
        }
        else{
            if(gebeAnzahlKarten() == 2 || MehrlingsCheck2(kleinstesBlatt.gebeZahl()) == 4 ) {
                return true;
            }
            else {
                return false;
            }
        }

    }

    public Listenelement entferneErstesAss() {
        //entfernt ein Ass und gibt diese zurück
        Listenelement y = kleinstesBlatt.entferneErstesAss();
        System.out.println(gebeSpieler() + " legt " + y.gebeFarbe() + "_" + y.gebeZahl());
        entferne2(y);
        return y;
    }

    /*public int gebeAnzahlKarten() {
    return kleinstesBlatt.gebeAnzahlKarten();
    }*/
    public Listenelement gebeKleinstesBlattPlusX(int x) {
        return kleinstesBlatt.gebeKleinstesBlattPlusX(x, kleinstesBlatt.gebeZahl());
    }

    public void setzeS(Listenelement sees) {
        s =  sees;
    }

    public void ReturnS (Listenelement s) {
        kleinstesBlatt.ReturnS(s);
    }

    public Listenelement WipeOutNumber2(int w) {
        //entfernt alle Karten mit Zahl w
        //switch Schleife, welche schaut, wie viele Mehrlinge vorhanden sind 
        Listenelement s = kleinstesBlatt.WipeOutNumber(w);
        int m = 0;
        switch(MehrlingsCheck2(s.gebeZahl()) ){
            case 4: 
            System.out.println(gebeSpieler() + " legt " + s.gebeFarbe() + "_" + s.gebeZahl());
            System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeZahl());
            System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeNachfolger().gebeZahl());
            System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl());
            entferne2(s.gebeNachfolger().gebeNachfolger().gebeNachfolger());
            entferne2(s.gebeNachfolger().gebeNachfolger());
            entferne2(s.gebeNachfolger());
            entferne2(s);
            case 3: 
            if(s.gebeNachfolger().gebeNachfolger() != null && s.gebeNachfolger() != null && s != null) {
                System.out.println(gebeSpieler() + " legt " + s.gebeFarbe() + "_" + s.gebeZahl());
                System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeZahl());
                System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeNachfolger().gebeZahl());
                entferne2(s.gebeNachfolger().gebeNachfolger());
                entferne2(s.gebeNachfolger());
                entferne2(s);
                m = 1;
            }
            break;
            case 2:
            System.out.println(gebeSpieler() + " legt " + s.gebeFarbe() + "_" + s.gebeZahl());
            System.out.println(gebeSpieler() + " legt " + s.gebeNachfolger().gebeFarbe() + "_" + s.gebeNachfolger().gebeZahl());

            entferne2(s.gebeNachfolger());
            entferne2(s);
            m = 2;
            break;

            case 1:
            System.out.println(gebeSpieler() + " legt " + s.gebeFarbe() + "_" + s.gebeZahl());
            entferne2(s);
            m = 1;
            break;

        }
        return s;
        // spieler2.WipeOutNumber(w);
        //spieler3.WipeOutNumber(w);

    }

    public void Zug() {
        kleinstesBlatt.Zug();
    }

    public Listenelement KleineresPaerchenDa() {
        //überprüft, ob bei kleinstesBlatt Mehrlinge vorhanden sind
        if(MehrlingsCheck2(kleinstesBlatt.gebeZahl()) > 1) {
            return kleinstesBlatt;
        }
        else{
            return null;
        }
    }

    public Listenelement MiniPaerchenDa() { 
        //bezieht sich hier auf 2er Pärchen von Mehrlingen
        if(MehrlingsCheck2(kleinstesBlatt.gebeZahl()) > 1) {
            return kleinstesBlatt;
        }
        else {
            return null;
        }
    }

    public Listenelement Strategie1() { 
        //entfernt alle Mehrlinge von kleinstesBlatt
        Listenelement u = null;
        if(KleineresPaerchenDa() != null) {
            u = KleineresPaerchenDa();
            if(MehrlingsCheck2(u.gebeZahl()) == 2) {
                System.out.println(gebeSpieler() + " legt: " + u.gebeFarbe() + "_" + u.gebeZahl());
                System.out.println(gebeSpieler() + " legt: " + u.gebeNachfolger().gebeFarbe() + "_" + u.gebeNachfolger().gebeZahl());
                entferne2(u.gebeNachfolger());
                entferne2(u);
            }
            if(MehrlingsCheck2(u.gebeZahl()) == 3) {
                System.out.println(gebeSpieler() + " legt: " + u.gebeFarbe() + "_" + u.gebeZahl());
                System.out.println(gebeSpieler() + " legt: " + u.gebeNachfolger().gebeFarbe() + "_" + u.gebeNachfolger().gebeZahl());
                System.out.println(gebeSpieler() + " legt: " + u.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + u.gebeNachfolger().gebeNachfolger().gebeZahl());
                entferne2(u.gebeNachfolger().gebeNachfolger());
                entferne2(u.gebeNachfolger());

                entferne2(u);
            }
            if(MehrlingsCheck2(u.gebeZahl()) == 4) {
                u = null;
            }

        }
        else {

            u = kleinstesBlatt;
            System.out.println(gebeSpieler() + " legt: " + u.gebeFarbe() + "_" + u.gebeZahl());
            entferne2(kleinstesBlatt);
        }
        return u;
    }

    public Listenelement Strategie2() { 
        //schaut, ob es Mehrlinge gibt, wenn man etwas höher zum Ausspielen geht, entfernt diese und gibt sie zurück
        Listenelement u = null;
        if(kleinstesBlatt.MiniPaerchenDa() != null) {
            u = kleinstesBlatt.MiniPaerchenDa( );
            System.out.println(gebeSpieler() + " legt: " + u.gebeFarbe() + "_" + u.gebeZahl());
            System.out.println(gebeSpieler() + " legt: " + u.gebeNachfolger().gebeFarbe() + "_" + u.gebeNachfolger().gebeZahl());
            entferne2(u.gebeNachfolger());
            entferne2(u);

        }
        else {

            u = null;
        }
        return u;
    }

    public Listenelement FallStrat3Keep(int mehrlingeMitte, int zahlMitte) {
        //überprüft, ob der Fall der Strategie eintritt und entfernt und returnt gegebenenfalls entsprechende Spielkarte
        

        Listenelement w = kleinstesBlatt.FallStrat3Keep(mehrlingeMitte,zahlMitte );

        if(w != null) {
            //System.out.println("                    debug_                    debug_zahl kleiner als zahlMitte + 2 in in FallStrat3A gefunden -> wird entfernt");
            System.out.println(gebeSpieler() + " legt " + w.gebeFarbe() + "_" + w.gebeZahl());
            entferne2(w);
            return w;
        }

        else{

            return null;

        }

    }
    
    public Listenelement FallStrat4Split(int mehrlingeMitte, int zahlMitte) {
                //überprüft, ob der Fall der Strategie eintritt und entfernt und returnt gegebenenfalls entsprechende Spielkarte
        Listenelement w = kleinstesBlatt.FallStrat4Split(mehrlingeMitte,zahlMitte );
        if(w != null) {
            System.out.println(gebeSpieler() + " legt " + w.gebeFarbe() + "_" + w.gebeZahl());
            entferne2(w);
            return w;
        }
        else{
            ausgesetzt = true;
            return null;
        }
    }

    public boolean FallStrat5HighDoubleKeep(int mehrlingeMitte, int zahlMitte) {
        //überprüft, ob der Fall der Strategie eintritt und entfernt gegebenenfalls die entsprechenden Spielkarte und returnt die erste.
        //true bedeutet, dass Strategie greift, und ausgesetzt wird, und somit keine sehr hohen Mehrling gespielt werden
        if(Legen4Passiv(mehrlingeMitte, zahlMitte) != null) {
            if(Legen4Passiv(mehrlingeMitte, zahlMitte).gebeZahl() >= 6 )  {
                /* ausgesetzt = true;
                System.out.println(gebeSpieler() + " setzt aus");*/
                return true;
            }
            else {
                return false;
            }
        }
        else{
            ausgesetzt = true;
            System.out.println(gebeSpieler() + " setzt aus");
            return true;
        }
    }

    public Listenelement FallStrat6HighDoublePlay(int mehrlingeMitte, int zahlMitte) { 
        //überprüft, ob der Fall der Strategie eintritt und entfernt gegebenenfalls die entsprechenden Spielkarte und returnt die erste.
        //true bedeutet, dass Strategie greift, und ausgesetzt wird, und somit sehr hohe Mehrling gespielt werden
        Listenelement v = Legen4Passiv(mehrlingeMitte, zahlMitte);
        if(v != null) {
            if(v.gebeZahl() >= 6 )  {
                //Legen4(mehrlingeMitte, zahlMitte);
                return v;
            }
            else {
                return null;
            }
        }
        else{
            ausgesetzt = true;
            System.out.println(gebeSpieler() + " setzt aus");
            return null;
        }
    }

    public Listenelement FallStrat7(int mehrlingeMitte, int zahlMitte) { 
                //überprüft, ob der Fall der Strategie eintritt und und returnt diese
                //die höhere Karte wird zuerst gespielt
        if(gebeAnzahlKarten() == 2 ) {
            return kleinstesBlatt.gebeNachfolger();
        }
        else {
            return null;
        }
    }

    public Listenelement FallStrat8(int mehrlingeMitte, int zahlMitte) { 
                    //überprüft, ob der Fall der Strategie eintritt und und returnt diese
                //die niedrigere Karte wird zuerst gespielt
        if(gebeAnzahlKarten() == 2 ) {
            return kleinstesBlatt;
        }
        else {
            return null;
        }
    }

    public Listenelement Strategie3Split(int mehrlingeMitte, int zahlMitte) {
        return FallStrat3Keep( mehrlingeMitte,  zahlMitte);
    }

    public Listenelement Strategie4(int mehrlingeMitte, int zahlMitte) {
        return FallStrat4Split( mehrlingeMitte,  zahlMitte);
    }

    public Listenelement Strategie5(int mehrlingeMitte, int zahlMitte) {
        if(gebeAnzahlKarten() == 2) {
            return Legen4(mehrlingeMitte, zahlMitte);
        }
        else{
            System.out.println(gebeSpieler() + " setzt aus");
            ausgesetzt = true;
            return null;
        }
    }

    public Listenelement Strategie6(int mehrlingeMitte, int zahlMitte) {

        return Legen4(mehrlingeMitte, zahlMitte);
    }

    public Listenelement Strategie7(int mehrlingeMitte, int zahlMitte) {
//entfernen der zurückgegebenen Spielkarte aus FallStrat7()

        if(mehrlingeMitte == 1) {
            if(kleinstesBlatt.gebeNachfolger().gebeZahl() > zahlMitte) {
                Listenelement z = kleinstesBlatt.gebeNachfolger();
                entferne2(z);
                return z;
            }
            else {
                if(kleinstesBlatt.gebeZahl() > zahlMitte) {
                    Listenelement z = kleinstesBlatt;
                    entferne2(z);
                    return z;
                }
                else{
                    return null;
                }
            }
        }
        else {
            return null; 
        }
    }

    public Listenelement Strategie8(int mehrlingeMitte, int zahlMitte) {
//entfernen der zurückgegebenen Spielkarte aus FallStrat8()
        if(mehrlingeMitte == 1) {
            if(kleinstesBlatt.gebeZahl() > zahlMitte) {
                Listenelement z = kleinstesBlatt;
                entferne2(z);
                return z;
            }
            else {
                if(kleinstesBlatt.gebeNachfolger().gebeZahl() > zahlMitte) {
                    Listenelement z = kleinstesBlatt.gebeNachfolger();
                    entferne2(z);
                    return z;
                }
                else{
                    return null;
                }
            }
        }
        else {
            return null; 
        }
    }

   

    public int MehrlingsCheck2(int confirm) {
        //gibt zurück wie viele Mehrlinge der Spieler von zahl confirm hat
        return kleinstesBlatt.MehrlingsCheck2(confirm);
    }



    

    public boolean HerzSechsCheck() {
//gibt zurück, ob ein Spieler die Herz 6 auf der Hand hat
        if(kleinstesBlatt.HerzSechsCheck() == true) {

            return true;
            
        }
        else{ return false;
        }
    }

}

