public class Spielkarte extends Listenelement{
    //int [] kartenNummer;  //Nummer in den 36 Spielkarten
    //Zahl = Z//*0 = 6 , 1 = 7 , 2 = 8 , 3 = 9 ; 4 = 10 
    //*5 = Bube , 6 = Dame , 7 = König , 8 = Ass
    int farbe;  // Farbe = F  * 0 = Blatt , 1 = Pik , 2 = Caro , 3 = Herz

    int zahl; //Zahl = Z//*0 = 6 , 1 = 7 , 2 = 8 , 3 = 9 ; 4 = 10 
    //*5 = Bube , 6 = Dame , 7 = König , 8 = Ass
    Listenelement nachfolger; //nachfolger in der Liste
    int temp = - 1; //allgemeine Zwischenspeicher-Variable
    int farbeSpeicher; //zum zwischenspeichern der Farbe einer Karte
    int mehrlingsSpeicher = -1; //zum speichern der Mehlinge einer Karte
    boolean ausgesetzt = false; //zum weitergeben an den Spieler
    boolean trumpf = false; //zum weitergeben an den Spieler
    int c = 0;
    Spielkarte vorAbschluss; 
    int n = 0;
    int bombe = -1; //zum weitergeben an den Spieler
    Listenelement s;

    public Spielkarte (int farbeNeu, int zahlNeu){
        
        nachfolger = null; //zunächst lose Karte...
        int [] kartenNummer = new int[35]; //...zufällig aus dem Stapel ausgewählt

        zahl = zahlNeu;
        farbe = farbeNeu;

    }
    //public int gebeZahl() {
    //  return iZahl;
    //}
    // public int gebeFarbe() {
    //   return iFarbe;
    //}

 

    public void HandPrintExtended() {
//Outprint der Hände der Spieler
        System.out.println("Spielkarte " + gebeFarbe() + " " + gebeZahl());
        nachfolger.HandPrintExtended();

    }

    public void setzeNachfolger(Listenelement l) {

        nachfolger = l;
    }

    public Listenelement gebeNachfolger(){
        return nachfolger;
    }

    public int gebeZahl() {
        return zahl;
    }

    public int gebeFarbe() {
        return farbe;
    }

    public boolean IstAbschluss () {
        return false;
    }

    public void setzeTemp(int t) {
        temp = t;
    }

    public int gebeTemp() {
        return temp;
    }

    public int gebeFarbeSpeicher() {
        return farbeSpeicher;
    }

    public void setzeAusgesetzt(boolean b) {
        ausgesetzt = b;
    }

    public boolean gebeAusgesetzt() {
        return ausgesetzt;
    }

    public int gebeMehrlingsSpeicher() {
        return mehrlingsSpeicher;
    }

    public boolean gebeTrumpf() {
        return trumpf;
    }

    public void setzeTrumpf(boolean t){
        trumpf = t;
    }

    public Listenelement gebeVorAbschluss() {
        return vorAbschluss;
    }

    public void setzeVorAbschluss(Spielkarte va) {
        vorAbschluss = va;
    }

    public int gebeC() {
        return c;
    }

    public int gebeBombe() {
        return bombe;
    }

    public Listenelement BombeNichtAusspielen() {
        //es wird die Karte die nach der Bombe in der Liste steht ausgespielt
        return this.gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeNachfolger();
    }

    public void SpielkarteErstellenHinzufuegen() {
    }

    public boolean SpielkarteHinzufuegen (Spielkarte k) {
        //soritiertes Einfügen einer neuen Karte in die Liste
        //rekursiver Aufruf, bis man die Stelle erreicht 
        // boolean: IstGrößer => true , IstKleiner => false
        if(k.gebeZahl() - this.gebeZahl() <= 0) {

            k.setzeNachfolger(this);

            return true;

        }

        else{ if(nachfolger.IstAbschluss() == false)  {
                boolean temp = nachfolger.SpielkarteHinzufuegen(k);
                if(temp == true) { 
                    nachfolger = k;
                }
            }
            else{this.setzeNachfolger(k);
                this.nachfolger.setzeNachfolger(new Abschluss());
            }

        }
        return false;
       
    }

   

    public void Austeilen () {
    }

    public int gebeAnzahlKarten () {
        //zählt rekurisv hoch
        return nachfolger.gebeAnzahlKarten() + 1;
    }

    public boolean setzeHB() {//HB = hoechstesBlatt => Wert bestimmt ob hoechstesBlatt bereits entfernt perd, denn dann muss der Nachfolger hoechstesBlatt werden

        return true;
    }

    public Listenelement entferne(int farbe, int zahl) {
        //entfernt eine Karte mit bestimmter zahl und farbe aus der Hand eines Spielers
        //rekurisver Aufruf bis man die Karte mit richtiger zahl und farbe gefunden hat => weißt dem vorgänger von this den nachfolger von this zu 
        int c = 0;

        if(this.gebeFarbe() == farbe && this.gebeZahl() == zahl) {

            return this;
            
        }
        else
        {
            if(nachfolger.IstAbschluss()) {
                return null;
            }
            else{
                if(nachfolger.gebeFarbe() == farbe && nachfolger.gebeZahl() == zahl)
                { this.setzeNachfolger(nachfolger.gebeNachfolger());
                    return nachfolger;
                }
                else{
                    nachfolger.entferne(farbe, zahl);
                    return nachfolger.entferne(farbe, zahl);
                }
            }

        }
    } 

    public Listenelement entferne2(Listenelement spielkarte ) {
        if(this.gebeFarbe() == farbe && this.gebeZahl() == zahl) {
//entfernt ein bestimmtes Listenelementaus der Hand eines Spielers und gibt es zurück
        //rekurisver Aufruf bis man die Karte mit richtiger zahl und farbe gefunden hat => weißt dem vorgänger von this den nachfolger von this zu 
            
            return this;

           

        }
        

        else
        {
            if(nachfolger.gebeFarbe() == spielkarte.gebeFarbe() && nachfolger.gebeZahl() == spielkarte.gebeZahl())
            { this.setzeNachfolger(nachfolger.gebeNachfolger());
                return nachfolger;
            }
            else{
                nachfolger.entferne(spielkarte.gebeFarbe(), spielkarte.gebeZahl());
                return nachfolger.entferne(spielkarte.gebeFarbe(), spielkarte.gebeZahl());
            }

        }
    }

    /*public Listenelement entferne3() {

    }
     */
    public int AktuelleZahlEntfernen() {
        //entfernt alle Karten mit der Zahl von this
        //rekursiver Aufruf, bis die Zahl sich ändert
        entferne2(this);

        if(nachfolger.gebeZahl() == this.gebeZahl()){

            nachfolger.AktuelleZahlEntfernen();
            entferne2(nachfolger);
            return nachfolger.AktuelleZahlEntfernen() + 1;
        }
        else{
            return 0;
        }

    }

    public Listenelement entferneErstesAss() {
        //returnt erstes Ass, auf das man trifft...
        //...beim rekursiven durchlaufen der Liste
       
        if(this.gebeZahl() == 8) {
            return this;

        }
        else {
            return nachfolger.entferneErstesAss();
        }

    }

   

    

    

    public Listenelement Legen4(int mehrlingeMitte, int zahlMitte) {
        //siehe Seminararbeit Kapitel 5.3.1
        if(this.gebeZahl() > zahlMitte) {
            //System.out.println("                    debug_Ia");
            return this;
        }
        else {
            //System.out.println("                    debug_Ib");
            if(zahlMitte + 4 >= nachfolger.gebeZahl()) {
                //System.out.println("                    debug_Iba");
                return nachfolger.Legen4(mehrlingeMitte, zahlMitte);

            }
            else {
                //System.out.println("                    debug_Ibb");
                ausgesetzt = true;
                return null;
            }
        }
    }

    public Listenelement gebeKleinstesBlattPlusX(int x, int kleinstesBlattZahl) {
        if(this.gebeZahl() == kleinstesBlattZahl + x) {
            return this;
        }
        else {
            return nachfolger.gebeKleinstesBlattPlusX(x, kleinstesBlattZahl);
        }
    }

    public  void setzeS(Listenelement l) {
        s = l;
    }

    public Listenelement ReturnS (Listenelement s) {
        return s;
    }

    public Listenelement gebeKleinstesBlatt() {
        return this;
    }

    public Listenelement WipeOutNumber(int v) {
        //entfernt alle Zahlen mit einer bestimmten Zahl
        //rekursiver Aufruf, bis  die Zahl sich geändert hat
        if(this.gebeZahl() == v) {

            //entferne2(this);
            System.out.println("Spielkarte " + this.gebeFarbe() + "_" + this.gebeZahl() + " entfernt");
            //entferne(this.gebeFarbe(), this.gebeZahl());
            return this;
        }
        else {

            return nachfolger.WipeOutNumber(v);

        }
    }

    public void Zug() {
    }

    /*public void MehrlingsCheckAvtiv() {
    int n = 1;
    if(nachfolger.gebeZahl() == zahl) {

    n = n + 1;
    entferne(nachfolger.gebeFarbe(),nachfolger.gebeZahl());
    }

    nachfolger.MehrlingsCheckAvtiv();
    }
     */

    public boolean HerzSechsCheck() {
        //Rekursiver Durchlauf mit Vergleichen, bis man die Herz 6 findet
        if(this.gebeFarbe() == 3 && this.gebeZahl() == 0) {

            return true;
        }
        else{ return nachfolger.HerzSechsCheck()
            ;
        }

    }

   

    public Listenelement MiniPaerchenDa() {
        //schaut, ob es Mehrlinge gibt, wenn man etwas höher zum Ausspielen geht und gibt sie zurück
        if(MehrlingsCheck2(this.gebeZahl()) == 3 || MehrlingsCheck2(this.gebeZahl()) == 4  ) {
            return null; //immer wenn strategie nicht greift null geturnen
        }
        else{

            if(this.gebeZahl() < 4) {
                if(MehrlingsCheck2(this.gebeZahl()) == 2 || MehrlingsCheck2(this.gebeZahl()) == 3) {
                    return this;
                }
                else {
                    return nachfolger.MiniPaerchenDa( );

                }
            }
            else {
                return null;
            }
        }

    }

    

    

    public Listenelement FallStrat3Keep(int mehrlingeMitte, int zahlMitte) {
        //überprüft, ob der Fall der Strategie eintritt und returnt gegebenenfalls entsprechende Spielkarte
        //funktioniert mit verschachtelten if-Abfragen mit entsprechende Überprüfungen zu Zahl und Mehrlingen, sollte nur aufgerufen werden, wenn MehrlingeMitte = 1
        Listenelement p = null;
        if(this.gebeZahl() > zahlMitte) {
            if(MehrlingsCheck2(this.gebeZahl()) == 1){
                if(this.gebeZahl() == 7 || this.gebeZahl() == 8) {
                    p = null;
                }
                else 
                    p = this;

            }
            else{
                if(zahlMitte + 4 >= nachfolger.gebeZahl()) {

                    p = nachfolger.FallStrat3Keep(mehrlingeMitte, zahlMitte);

                }
                else {
                    if(p != null) {

                        p.setzeAusgesetzt(true);
                        p = null;
                    }
                }
            }
        }

        else {
            if(zahlMitte + 4 >= nachfolger.gebeZahl()) {
                //System.out.println("                    debug_Iba");
                p = nachfolger.FallStrat3Keep(mehrlingeMitte, zahlMitte);

            }
            else {
                if(p != null) {
                    //System.out.println("                    debug_Ibb");
                    p = null;
                    p.setzeAusgesetzt(true);
            }
            }
        }
        return p;

    }

    public Listenelement FallStrat4Split(int mehrlingeMitte, int zahlMitte) {
                        //überprüft, ob der Fall der Strategie eintritt und entfernt und returnt gegebenenfalls entsprechende Spielkarte
        //funktioniert mit verschachtelten if-Abfragen mit entsprechende Überprüfungen zu Zahl und Mehrlingen, sollte nur aufgerufen werden, wenn MehrlingeMitte = 1
        if(this.gebeZahl() > zahlMitte) {
            if(this.gebeZahl() != 7 && this.gebeZahl() != 8 && MehrlingsCheck2(this.gebeZahl()) == 1) { //=>kein König oder Ass Legen
                return this;
            }
            else{
                if(zahlMitte + 4 >= nachfolger.gebeZahl()) {
                    //System.out.println("                    debug_Iba");
                    return nachfolger.FallStrat4Split(mehrlingeMitte, zahlMitte);

                }
                else {
                    //System.out.println("                    debug_Ibb");
                    ausgesetzt = true;
                    return null;
                }
            }
        }

        else{
            if(zahlMitte + 4 >= nachfolger.gebeZahl()) {
                //System.out.println("                    debug_Iba");
                return nachfolger.FallStrat4Split(mehrlingeMitte, zahlMitte);

            }
            else {
                //System.out.println("                    debug_Ibb");
                ausgesetzt = true;
                return null;
            }

        }
    }

    

    public int MehrlingsCheck2 (int confirm) {
        //gibt zurück wie viele Mehrlinge von der zahl confirm der Spieler in dem man sich befindet hat
//rekursiver Durchlauf mit Addition +1 wenn die zahl von this gleich confirm ist
       
        if(this.gebeZahl() == confirm) {

            return nachfolger.MehrlingsCheck2(confirm) + 1;

        }
        else{ 
            return nachfolger.MehrlingsCheck2(confirm);
        }

    }

    

}
