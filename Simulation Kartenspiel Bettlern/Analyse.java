public class Analyse {
    int winS1 = 0; //Variable, welche hochzählt wie oft Spieler 1 Spieler 2 geschlagen hat
    int winS2 = 0; //Variable, welche hochzählt wie oft Spieler 2 Spieler 1 geschlagen hat
    Spiel spiel; 

    public Analyse() {
    }

    public void Analyse100x(int StrategieS1, int StrategieS2) {
        for(int i = 1; i <= 100; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            Spiel spiel = new Spiel();
            int a = spiel.A_Spielfluss4(StrategieS1, StrategieS2,(int) ((Math.random()) * 8) + 1);

            if(a == 1) {
                winS1 = winS1 + 1;
            }
            if(a == 2) {
                winS2 = winS2 + 1;
            }
            if(i == 99) {
                break;
            }
            spiel = null;
        }

    }

    public void Analyse100x2(int StrategieS1, int StrategieS2) {
        for(int i = 1; i <= 100; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,(int) ((Math.random()) * 8) + 1);
            setStats();
            spiel = null;
        }

        giveStats(100, StrategieS1, StrategieS2);
    }

    public void Analyse100N(int StrategieS1, int StrategieS2, int StrategieS3) {
        for(int i = 1; i <= 100; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();

            spiel.A_Spielfluss4(StrategieS1, StrategieS2,StrategieS3);
            setStats();
            spiel = null;
        }

        giveStats(100, StrategieS1, StrategieS2);
    }

    public void AnalyseN(int n, int StrategieS1, int StrategieS2, int StrategieS3) {
        //for Schleife, welche n mal das Spiel wiederholt, die nach jeder Runde die stats einträgt und diese dann als Outprint herausgibt
        for(int i = 1; i <= n; i++) {
            //System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,StrategieS3);
            setStats();
            spiel = null;
        }
System.out.println("Operation N");
        giveStats(n, StrategieS1, StrategieS2);
    }

    public void AnalyseP(int n, int StrategieS1, int StrategieS2) {
                //for Schleife, welche n mal das Spiel wiederholt, die nach jeder Runde die stats einträgt und diese dann als Outprint herausgibt
        for(int i = 1; i <= n; i++) {
            //System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,(int) ((Math.random()) * 8) + 1);
            setStats();
            spiel = null;
        }
System.out.println("Operation P");
        giveStats(n, StrategieS1, StrategieS2);
    }
    public void AnalyseNx10Millionen(int StrategieS1, int StrategieS2, int StrategieS3) {
        //for Schleife, welche n mal das Spiel wiederholt, die nach jeder Runde die stats einträgt und diese dann als Outprint herausgibt
        for(int i = 1; i <= 10000000; i++) {
            //System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,StrategieS3);
            setStats();
            spiel = null;
        }
System.out.println("Operation N");
        giveStats(10000000, StrategieS1, StrategieS2);
    }

    public void AnalyseP10Millionen(int StrategieS1, int StrategieS2) {
                //for Schleife, welche n mal das Spiel wiederholt, die nach jeder Runde die stats einträgt und diese dann als Outprint herausgibt
        for(int i = 1; i <= 10000000; i++) {
            //System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,(int) ((Math.random()) * 8) + 1);
            setStats();
            spiel = null;
        }
System.out.println("Operation P");
        giveStats(10000000, StrategieS1, StrategieS2);
    }

    public void Analyse1000x3(int StrategieS1, int StrategieS2, int StrategieS3) {
        for(int i = 1; i <= 1000; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,StrategieS3);
            setStats();
            spiel = null;
        }

        giveStats(1000, StrategieS1, StrategieS2);
    }

    public void Analyse10x2(int StrategieS1, int StrategieS2) {
        for(int i = 1; i <= 10; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,(int) ((Math.random()) * 8) + 1);
            setStats();
            spiel = null;
        }

        giveStats(10, StrategieS1, StrategieS2);
    }

    public void Analyse10x3(int StrategieS1, int StrategieS2, int StrategieS3) {
        for(int i = 1; i <= 10; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2,StrategieS3);
            setStats();
            spiel = null;
        }

        giveStats(10, StrategieS1, StrategieS2);
    }

    public void Analyse50x2(int StrategieS1, int StrategieS2) {
        for(int i = 1; i <= 50; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2, (int) ((Math.random()) * 8) + 1);
            setStats();
            spiel = null;
        }

        giveStats(50, StrategieS1, StrategieS2);
    }

    public void Analyse50x3(int StrategieS1, int StrategieS2, int StrategieS3) {
        for(int i = 1; i <= 50; i++) {
            System.out.println("Spiel Nummer " + i + " wurde erstellt");
            spiel = new Spiel();
            spiel.A_Spielfluss4(StrategieS1, StrategieS2, StrategieS3);
            setStats();
            spiel = null;
        }

        giveStats(50, StrategieS1, StrategieS2);
    }

    public void setStats() {
        if(spiel.gebePlatz_1() == spiel.gebeSpieler(1)) {
            winS1 = winS1 + 1;
        }
        else {
            if(spiel.gebePlatz_1() == spiel.gebeSpieler(2)) {
                winS2 = winS2 + 1;
            }
            else{
                if(spiel.gebePlatz_2() == spiel.gebeSpieler(1) && spiel.gebePlatz_1() != spiel.gebeSpieler(2)) {
                    winS1 = winS1 + 1;
                }
                else {

                    winS2 = winS2 + 1;
                }

            }
        }
    }

    public void giveStats(int n, int StrategieS1, int StrategieS2) {
        System.out.println("Es wurden " + n + " Spiele gespielt");
        System.out.println("spieler1(Strategie " + + StrategieS1 + ") hat " + winS1 + " mal die höhere Platzierung als spieler2 erreicht");
        System.out.println("spieler2(Strategie " + + StrategieS2 + ") hat " + winS2 + " mal die höhere Platzierung erreicht als spieler1 erreicht");
        //System.out.println("spieler1 Wahrschenlichkeit einer höheren Platzierung als spieler2: BeatWSK: " + winS1 / n);
        //System.out.println("spieler1 Wahrschenlichkeit einer höheren Platzierung als spieler2: BeatWSK: " + winS2 / n);
        
        System.out.println("Kurzfassung:");
        System.out.println("Spiele: " + n);
        System.out.println("spieler1(Strategie " + + StrategieS1 + "): BeatS2: "+ winS1);
                System.out.println("spieler2(Strategie " + + StrategieS2 + "): BeatS1: "+ winS2);
       // System.out.println("spieler1 BeatWSK: " + winS1/n);
       // System.out.println("spieler2 BeatWSK: " + winS2/n);
        
    }

}