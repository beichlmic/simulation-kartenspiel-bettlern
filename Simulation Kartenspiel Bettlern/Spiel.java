import java.util.*;

public class Spiel { 
    int zahl; 
    int farbe;
    int reihe;
    int fertigCounter = 0; //zählt, wie viele Spieler bereits fertig sind
    int e;
    int siegS1 = 0; 
    int siegS2 = 0;
    int rek = 1;
    int Nummer;


    Spieler spieler1 = new Spieler("spieler1");
    Spieler spieler2 = new Spieler("spieler2");
    Spieler spieler3 = new Spieler("spieler3");

    Spieler Platz_1 = null;

    Spieler Platz_2 = null;

    Spieler Platz_3 = null;

    Mitte mitte = new Mitte();


    List kartenstapel = new ArrayList<Spielkarte>();

    public Spiel(){ 
        //Konstruktur der die Klasse erstellt und die Methode Austeilen aufruft
        

       Austeilen ();
        
    }

    public Spieler gebeSpieler(int nr) { 
        //gibt den Spieler mit der Nummer nr im Parameter zurück 
        if(nr == 1) {
            return spieler1;
        }
        else{
            if(nr == 2) {
                return spieler2;
            }
            else{

                if(nr == 3) {
                    return spieler3;
                }
                else {
                    return null;
                }
            }

        }
    }
    public void A_EntferneErstesAss(){ //Test-Methode
        
        spieler1.entferneErstesAss();
                spieler2.entferneErstesAss();
                        spieler3.entferneErstesAss();
                        GeneralHandprint();

    }
     public void A_Strat1Test(int StrategieS1, int StrategieS2, int StrategieS3) {
        spieler2.setzeAusgesetzt(true);
        spieler3.setzeAusgesetzt(true);
        Zug4S1( StrategieS1,  StrategieS2,  StrategieS3);
    }
    public void A_Strat2Test(int StrategieS1, int StrategieS2, int StrategieS3) {
        spieler1.setzeAusgesetzt(true);
        spieler3.setzeAusgesetzt(true);
        Zug4S2( StrategieS1,  StrategieS2,  StrategieS3);
    }
    public void A_StratXS1Test(int x, int y, int StrategieS1, int StrategieS2, int StrategieS3) {
        
       mitte.setzeMitte(x,y);
        Zug4S1( StrategieS1,  StrategieS2,  StrategieS3);
    }
    public void A_StratXS2Test(int x, int y,int StrategieS1, int StrategieS2, int StrategieS3) {
              mitte.setzeMitte(x,y);
        Zug4S2( StrategieS1,  StrategieS2,  StrategieS3);
    }
    public void A_Strat7S2Test(int x, int y,int StrategieS1, int StrategieS2, int StrategieS3) {
        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
              mitte.setzeMitte(x,y);
              GeneralHandprint();
        Zug4S2( StrategieS1,  StrategieS2,  StrategieS3);
    }
    public void A_Strat8S2Test(int x, int y,int StrategieS1, int StrategieS2, int StrategieS3) {
        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
              mitte.setzeMitte(x,y);
                            GeneralHandprint();
        Zug4S2( StrategieS1,  StrategieS2,  StrategieS3);
    }
    
    

    public Spieler gebePlatz_1() { 
        //gibt Platz_1 zurück
        return Platz_1;
    }

    public Spieler gebePlatz_2() { 
        //gibt Platz_2 zurück
        return Platz_2;
    }

    public Spieler gebePlatz_3() { 
        //gibt Platz_3 zurück
        return Platz_3;
    }

    public void DecksErstellen() { 
        //erstellt den Kartenstapel
        for (int i = 0; i<4; i++){
            for(int j = 0; j<9; j++){
                kartenstapel.add(new Spielkarte(i,j));
            }
        }
       
    }

  

    public void Austeilen () { 
        //teilt per Aufruf der MethodeDecksErstellen() und einer for-Schleife  Spielkarten aus

        DecksErstellen();
       

        for(int i = 0; i < 12 ; i++) {
            int x = (int) ((Math.random()) * kartenstapel.size());
            Spielkarte k = (Spielkarte) kartenstapel.remove(x);

            spieler1.SpielkarteErstellenHinzufuegen(k);
            System.out.println("Spielkarte " + spieler1.SpielkarteReturn(k).gebeFarbe() + " " + spieler1.SpielkarteReturn(k).gebeZahl() + " wurde an Spieler 1 ausgeteilt");

            x = (int) ((Math.random()) * kartenstapel.size());
            k = (Spielkarte) kartenstapel.remove(x);

            spieler2.SpielkarteErstellenHinzufuegen(k);

            System.out.println("Spielkarte " + spieler2.SpielkarteReturn(k).gebeFarbe() + " " + spieler2.SpielkarteReturn(k).gebeZahl() + " wurde an Spieler 2 ausgeteilt");

            x = (int) ((Math.random()) * kartenstapel.size());
            k = (Spielkarte) kartenstapel.remove(x);

            spieler3.SpielkarteErstellenHinzufuegen(k);

            System.out.println("Spielkarte " + spieler2.SpielkarteReturn(k).gebeFarbe() + " " + spieler2.SpielkarteReturn(k).gebeZahl() + " wurde an Spieler 3 ausgeteilt");
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Karten der Spieler:");
        System.out.println("");

        MitteOutprint();

        GeneralHandprint();

    }

    

   

    
   

    

    

    public int A_Spielfluss4(int StrategieS1,int StrategieS2,int  StrategieS3) { 
        //beginnt das Spiel per Ausspielen der Herz 6 und ruft Spielflussrek4() auf
        if(spieler1.HerzSechsCheck() == true) {
            spieler1.entferne(3,0);
            //nur entfernen von einem mehrling also beim ausspielen von herz 6
            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler1 mit Herz 6 spielt 3_0 aus");

            ZugS2();
            ZugS3();

        }
        if(spieler2.HerzSechsCheck() == true) {
            spieler2.entferne(3,0);

            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler2 mit Herz 6 spielt 3_0 aus");

            ZugS3();

        }
        if(spieler3.HerzSechsCheck() == true) {
            spieler3.entferne(3,0);

            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler3 mit Herz 6 spielt 3_0 aus");

        }
        int temp = SpielflussRek4(StrategieS1, StrategieS2, StrategieS3);
        if(Platz_1 == spieler1) {
            siegS1 = siegS1 + 1;
        }
        else {
            if(Platz_1 == spieler2) {
                siegS2 = siegS2 + 1;
            }
            else{
                if(Platz_2 == spieler1 && Platz_1 != spieler2) {
                    siegS1 = siegS1 + 1;
                }
                else {

                    siegS2 = siegS2 + 1;
                }

            }
        }
        return temp;

    }

    public void FertigKlauselS1(int StrategieS1,int  StrategieS2,int  StrategieS3) { 
        //Klausel für den Fall, dass ein Spieler fertig ist 
        //Überprüft die Variable fertig bei allen Spielern
        if(spieler3.gebeFertig())  {
            Zug4S1(StrategieS1, StrategieS2, StrategieS3);
            Zug4S2(StrategieS1, StrategieS2, StrategieS3);
            if(spieler2.gebeAusgesetzt()) {
                AusspielenS1();
                Zug4S2(StrategieS1, StrategieS2, StrategieS3);
                SpielflussRek4S1vS2(StrategieS1, StrategieS2, StrategieS3);
            }
            else {
                if(spieler1.gebeAusgesetzt()) {
                    AusspielenS2();
                }
                else {
                    SpielflussRek4(StrategieS1, StrategieS2, StrategieS3);
                }
            }

        }

    }

    public void FertigKlauselS2(int StrategieS1, int StrategieS2, int StrategieS3) { 
        //Klausel für den Fall, dass ein Spieler fertig ist 
        //Überprüft die Variable fertig bei allen Spielern
        if(spieler1.gebeFertig())  {
            Zug4S2(StrategieS1, StrategieS2, StrategieS3);
            Zug4S3(StrategieS1, StrategieS2, StrategieS3);
            if(spieler3.gebeAusgesetzt()) {
                AusspielenS2();
                Zug4S3(StrategieS1, StrategieS2, StrategieS3);
                SpielflussRek4S2vS3(StrategieS1, StrategieS2, StrategieS3);
            }
            else {
                if(spieler2.gebeAusgesetzt()) {
                    Ausspielen4S3(StrategieS1, StrategieS2, StrategieS3);
                }
                else {
                    SpielflussRek4(StrategieS1, StrategieS2, StrategieS3);
                }
            }

        }
    }

    public void FertigKlauselS3(int StrategieS1, int StrategieS2, int StrategieS3) {  
        //Klausel für den Fall, dass ein Spieler fertig ist 
        //Überprüft die Variable fertig bei allen Spielern
        if(spieler2.gebeFertig())  {

            Zug4S3(StrategieS1, StrategieS2, StrategieS3);
            Zug4S1(StrategieS1, StrategieS2, StrategieS3);
            if(spieler1.gebeAusgesetzt()) {
                AusspielenS3();
                Zug4S1(StrategieS1, StrategieS2, StrategieS3);
                SpielflussRek4S1vS3(StrategieS1, StrategieS2, StrategieS3);
            }
            else {
                if(spieler3.gebeAusgesetzt()) {
                    Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
                }
                else {
                    SpielflussRek4(StrategieS1, StrategieS2, StrategieS3);
                }
            }

        }
    }

    public int SpielflussRek4(int StrategieS1,int  StrategieS2,int  StrategieS3) { 
        //erweiterte rekursive Methode, welche tatsächlich für den Spielfluss verantwortlich ist und in A_Spielfluss4 aufgerufen wird 
        //Sie ruft mit einer for-Wiederholung alle Spieler Runde für Runde zu ihrem Zug auf, bis zwei fertig sind 
        if(spieler1.gebeFertig() == false) {

            if(fertigCounter == 1) {
                FertigKlauselS1(StrategieS1,  StrategieS2, StrategieS3);
            }

            else{
                //System.out.println("                    debug_SpielflussRek4_ZugSpieler1 ");
                Zug4S1(StrategieS1,  StrategieS2, StrategieS3);
            }

        }
        else {
            if(spieler1.gebeSchonFertig() == 1) {
                //System.out.println("                    debug_ " + spieler1.gebeSchonFertig());
                //System.out.println("                    debug_spieler1.gebeSchonFertig() == 1");
            }
            else{
                spieler1.setzeSchonFertig(spieler1.gebeSchonFertig() + 1);
                //System.out.println("                    debug_spieler1.setzeSchonFertig(spieler1.gebeSchonFertig() + 1)");

                //System.out.println("                    debug_spieler1.gebeSchonFertig() != 1");
                if (spieler2.gebeFertig() == false && spieler3.gebeFertig() == false) {
                    Platz_1 = spieler1;
                    System.out.println("spieler1 ist fertig und wurde Platz_1");
                    fertigCounter = fertigCounter + 1;
                    //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                }
                else {
                    if(spieler2.gebeFertig() || spieler3.gebeFertig()) {
                        Platz_2 = spieler1;
                        System.out.println("spieler1 ist fertig und wurde Platz_2");
                        fertigCounter = fertigCounter + 1;
                        //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                    }

                }
            }
        }
            if(Fertig()) {
                return Auswertung();
            }else{

        
                if(spieler2.gebeFertig() == false) {
                    //System.out.println("                    debug_SpielflussRek4_ZugSpieler ");
                    if(fertigCounter == 1) {
                        FertigKlauselS2(StrategieS1,  StrategieS2, StrategieS3);
                    }

                    else{
                        //System.out.println("                    debug_SpielflussRek4_ZugSpieler2 ");
                        Zug4S2(StrategieS1,  StrategieS2, StrategieS3);
                    }
                }
                else {
                    //System.out.println("                    debug_ " + spieler2.gebeSchonFertig());
                    if(spieler2.gebeSchonFertig() == 1) {
                        //System.out.println("                    debug_spieler1.gebeSchonFertig() == 1");
                    }
                    else{
                        spieler2.setzeSchonFertig(spieler2.gebeSchonFertig() + 1);
                        //System.out.println("                    debug_spieler1.setzeSchonFertig(spieler2.gebeSchonFertig() + 1)");

                        //System.out.println("                    debug_spieler2.gebeSchonFertig() != 1");
                        if (spieler1.gebeFertig() == false && spieler3.gebeFertig() == false) {
                            Platz_1 = spieler2;
                            System.out.println("spieler2 ist fertig und wurde Platz_1");
                            fertigCounter = fertigCounter + 1;
                            //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                        }
                        else {
                            if(spieler1.gebeFertig() || spieler3.gebeFertig()) {
                                Platz_2 = spieler2;
                                System.out.println("spieler2 ist fertig und wurde Platz_2");
                                fertigCounter = fertigCounter + 1;
                                //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                            }
                        }

                    }
                }
            }
            if(Fertig()) {
                return Auswertung();
            }
            else{
                if(spieler3.gebeFertig() == false) {
                    if(fertigCounter == 1) {
                        FertigKlauselS3(StrategieS1,  StrategieS2, StrategieS3);
                    }

                    else{
                        //System.out.println("                    debug_SpielflussRek4_ZugSpieler3 ");
                        Zug4S3(StrategieS1,  StrategieS2, StrategieS3);
                    }
                }
                else {  
                    //System.out.println("                    debug_ " + spieler3.gebeSchonFertig());

                    if(spieler3.gebeSchonFertig() == 1) {
                        //System.out.println("                    debug_spieler3.gebeSchonFertig() == 1");
                    }
                    else{
                        spieler3.setzeSchonFertig(spieler3.gebeSchonFertig() + 1);
                        //System.out.println("                    debug_spieler3.setzeSchonFertig(spieler1.gebeSchonFertig() + 1)");

                        //System.out.println("                    debug_spieler3.gebeSchonFertig() != 1");
                        if (spieler1.gebeFertig() == false && spieler2.gebeFertig() == false) {
                            Platz_1 = spieler3;
                            System.out.println("spieler3 ist fertig und wurde Platz_1");
                            fertigCounter = fertigCounter + 1;
                            //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                        }
                        else {
                            if(spieler1.gebeFertig() || spieler2.gebeFertig()) {
                                Platz_2 = spieler3;
                                System.out.println("spieler3 ist fertig und wurde Platz_2");
                                fertigCounter = fertigCounter + 1;
                                //System.out.println("                    debug_fertigCounter = fertigCounter + 1");
                            }
                        }
                    }
                }
            }
        
        if(Fertig()) {
            return Auswertung();
        }
        else{
            return SpielflussRek4(StrategieS1, StrategieS2, StrategieS3);
        }

    }

    public int Auswertung() { 
        //Methode, welche wenn das Spiel vorbei ist zurück gibt, ob spieler1 höher platziert ist (1), oder ob spieler2 höher platziert ist (2)
        if(Platz_1 == spieler1) {
            return 1;
        }
        else {
            if(Platz_1 == spieler2) {
                return 2;
            }
            else{
                if(Platz_2 == spieler1 && Platz_1 != spieler2) {
                    return 1;
                }
                else {

                    return 2;
                }

            }
        }
    }

    public boolean Fertig() { 
        //Methode, welche überprüft, ob das Spiel vorbei ist
        if(fertigCounter == 2) {

            System.out.println("erster finito");
            System.out.println("");

            System.out.println("Scoreboard:");
            if(Platz_1 != null) {
                System.out.println("Platz_1: " + Platz_1.gebeSpieler());
            }
            else{
                System.out.println("Platz_1:_______ ");
            }
            if(Platz_2 != null) {
                System.out.println("Platz_2: " + Platz_2.gebeSpieler());
            }
            else{
                System.out.println("Platz_2:_______ ");
            } if(Platz_3 != null) {
                System.out.println("Platz_3: " + Platz_3.gebeSpieler());
            }
            else{
                System.out.println("Platz_3:_______ ");
            }
            return true;
        }
        else{
            return false;
        }
    }

    public void Spielfluss2() { 
        //Methode für Spielfluss mit allen Spielern mit Dummy-Strategie bzw. Strategie 0, welche das Spiel mit Ausspielen von Herz 6 anfängt und Spielfluss2Rek() aufruft
        if(spieler1.HerzSechsCheck() == true) {
            spieler1.entferne(3,0);
            //nur entfernen von einem mehrling also beim ausspielen von herz 6
            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler1 mit Herz 6 spielt 3_0 aus");

            ZugS2();
            ZugS3();

        }
        if(spieler2.HerzSechsCheck() == true) {
            spieler2.entferne(3,0);

            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler2 mit Herz 6 spielt 3_0 aus");

            ZugS3();

        }
        if(spieler3.HerzSechsCheck() == true) {
            spieler3.entferne(3,0);

            mitte.setzeZahl(0);
            mitte.setzeMehrlinge(1);
            System.out.println("spieler3 mit Herz 6 spielt 3_0 aus");

        }
        SpielflussRek2();
    }

   

    public void SpielflussRek4S1vS2(int StrategieS1, int StrategieS2, int StrategieS3) { 
       //Methode, welche für das 1v1 von zwei Spielern zuständig ist, sobald einer gesiegt hat 
       //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler2.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler2.gebeAusgesetzt() -> Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);

        }
        else{
            //System.out.println("                    debug_spieler2.gebeAusgesetzt() == false -> DraufLegen4S1(StrategieS1, StrategieS2, StrategieS3);;");
            DraufLegen4S1(StrategieS1, StrategieS2, StrategieS3);

        }
        //System.out.println("                    debug_spieler2 am Zug im 1v1");
        if(spieler1.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler1.gebeAusgesetzt() -> Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
        }
        else{
            //System.out.println("                    debug_spieler1.gebeAusgesetzt() == false -> DraufLegen4S2(StrategieS1, StrategieS2, StrategieS3");
            DraufLegen4S2(StrategieS1, StrategieS2, StrategieS3);
        }
        if(spieler1.gebeFertig() == false && spieler2.gebeFertig() == false) {
            SpielflussRek4S1vS2(StrategieS1, StrategieS2, StrategieS3);
        }
        else {
            if(spieler1.gebeFertig()) {
                Platz_2 = spieler1;
                Platz_3 = spieler2;
            }
            if(spieler2.gebeFertig()) {
                Platz_2 = spieler2;
                Platz_3 = spieler1;
            }
        }

    }

    public void SpielflussRek4S2vS3(int StrategieS1, int StrategieS2, int StrategieS3) { 
        //Methode, welche für das 1v1 von zwei Spielern zuständig ist, sobald einer gesiegt hat 
        //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler3.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler3.gebeAusgesetzt() -> Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
        }
        else{
            //System.out.println("                    debug_spieler3.gebeAusgesetzt() == false -> DraufLegen4S2(StrategieS1, StrategieS2, StrategieS3);");
            DraufLegen4S2(StrategieS1, StrategieS2, StrategieS3);
        }
        if(spieler2.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler2.gebeAusgesetzt() -> Ausspielen4S3(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S3(StrategieS1, StrategieS2, StrategieS3);

        }
        else{
            //System.out.println("                    debug_spieler2.gebeAusgesetzt() == false ->             DraufLegen4S3(StrategieS1, StrategieS2, StrategieS3);");
            DraufLegen4S3(StrategieS1, StrategieS2, StrategieS3);
        }
        if(spieler2.gebeFertig() == false && spieler3.gebeFertig() == false) {
            SpielflussRek4S2vS3(StrategieS1, StrategieS2, StrategieS3);

        }
        else {
            if(spieler2.gebeFertig()) {
                Platz_2 = spieler2;
                Platz_3 = spieler3;
            }
            if(spieler3.gebeFertig()) {
                Platz_2 = spieler3;
                Platz_3 = spieler2;
            }
        }
    }

    public void SpielflussRek4S1vS3(int StrategieS1, int StrategieS2, int StrategieS3) { 
        //Methode, welche für das 1v1 von zwei Spielern zuständig ist, sobald einer gesiegt hat 
        //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler3.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler3.gebeAusgesetzt() -> Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
        }
        else{
            //System.out.println("                    debug_spieler3.gebeAusgesetzt() == false -> DraufLegen4S1(StrategieS1, StrategieS2, StrategieS3);;");
            DraufLegen4S1(StrategieS1, StrategieS2, StrategieS3);

        }
        //System.out.println("                    debug_spieler3 am Zug im 1v1");
        if(spieler1.gebeAusgesetzt()) {
            //System.out.println("                    debug_spieler1.gebeAusgesetzt() -> Ausspielen4S3(StrategieS1, StrategieS2, StrategieS3);");
            Ausspielen4S3(StrategieS1, StrategieS2, StrategieS3);
        }
        else{
            //System.out.println("                    debug_spieler1.gebeAusgesetzt() == false -> DraufLegen4S3(StrategieS1, StrategieS2, StrategieS3);;");
            DraufLegen4S3(StrategieS1, StrategieS2, StrategieS3);
        }
        if(spieler1.gebeFertig() == false && spieler3.gebeFertig() == false) {
            SpielflussRek4S1vS3(StrategieS1, StrategieS2, StrategieS3);

        }
        else {
            if(spieler1.gebeFertig()) {
                Platz_2 = spieler1;
                Platz_3 = spieler3;
            }
            if(spieler3.gebeFertig()) {
                Platz_2 = spieler3;
                Platz_3 = spieler1;
            }
        }
    }

    public void SpielflussRekS1vS2() { 
        //Methode, welche für das 1v1 von zwei Spielern mit Dummy-Strategie zuständig ist, sobald einer gesiegt hat 
        //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler2.gebeAusgesetzt()) {
            AusspielenS1();
        }
        else{
            DraufLegenS1();
        }

        if(spieler1.gebeAusgesetzt()) {
            AusspielenS2();
        }
        else{
            DraufLegenS2();
        }
        if(spieler1.gebeFertig() == false && spieler2.gebeFertig() == false) {
            SpielflussRekS1vS2();
        }
        else {
            if(spieler1.gebeFertig()) {
                Platz_2 = spieler1;
                Platz_3 = spieler2;
            }
            if(spieler2.gebeFertig()) {
                Platz_2 = spieler2;
                Platz_3 = spieler1;
            }
        }
    }

    public void SpielflussRekS2vS3() { 
        //Methode, welche für das 1v1 von zwei Spielern mit Dummy-Strategie zuständig ist, sobald einer gesiegt hat 
        //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler3.gebeAusgesetzt()) {
            AusspielenS2();
        }
        else{
            DraufLegenS2();
        }
        if(spieler2.gebeAusgesetzt()) {
            AusspielenS3();
        }
        else{
            DraufLegenS3();
        }
        if(spieler2.gebeFertig() == false && spieler3.gebeFertig() == false) {
            SpielflussRekS2vS3();
        }
        else {
            if(spieler2.gebeFertig()) {
                Platz_2 = spieler2;
                Platz_3 = spieler3;
            }
            if(spieler3.gebeFertig()) {
                Platz_2 = spieler3;
                Platz_3 = spieler2;
            }
        }

    }

    public void SpielflussRekS1vS3() { 
        //Methode, welche für das 1v1 von zwei Spielern mit Dummy-Strategie zuständig ist, sobald einer gesiegt hat 
        //funktioniert durch rekursives Aufrufen von den Methoden DrauflegenSn() bzw. AusspielenSn()
        if(spieler3.gebeAusgesetzt()) {
            AusspielenS1();
        }
        else{
            DraufLegenS1();
        }
        if(spieler1.gebeAusgesetzt()) {
            AusspielenS3();
        }
        else{
            DraufLegenS3();
        }
        if(spieler1.gebeFertig() == false && spieler3.gebeFertig() == false) {
            SpielflussRekS1vS3();
        }
        else {
            if(spieler1.gebeFertig()) {
                Platz_2 = spieler1;
                Platz_3 = spieler3;
            }
            if(spieler3.gebeFertig()) {
                Platz_2 = spieler3;
                Platz_3 = spieler1;
            }
        }
    }

    public void SpielflussRek2() {
        //Methode für Spielfluss mit allen Spielern mit Dummy-Strategie bzw. Strategie 0
        //Wiederholt sich rekursiv bis ein Spieler Platz_1 ist 
        if(spieler1.gebeKleinstesBlatt() != null && spieler2.gebeKleinstesBlatt() != null && spieler3.gebeKleinstesBlatt() != null) {
            ZugS1();
            ZugS2();
            ZugS3();
            SpielflussRek2();
        }
        else{
            if(spieler1.gebeKleinstesBlatt() == null) {
                Platz_1 = spieler1;
                if(spieler2.gebeKleinstesBlatt() != null && spieler3.gebeKleinstesBlatt() != null) {
                    ZugS2();
                    ZugS3();
                    SpielflussRek2();
                }
                else {

                }
            }
        }
    }

    public void DreiZüge() {

        ZugS1();
        ZugS2();
        ZugS3();
    }

    

    public void ZugS1_1() { 
        //bedeuted Ausführug eines Zuges eine Spielers unter Strategie Ia
        //Prinzp: if-Abfragung ob spieler Trumpf hat, und dementsprechend einen Zug ausführen und Mitte ändern
        if(TrumpfCheckS1() == false) {
            ZugS1();
        }
        else{
            Listenelement v = spieler1.Strategie1();
            if(v != null) {
                MitteSetzen(2,v.gebeZahl());
            }
            else{
                ZugS1();
            }
        }

    }

    public void Zug4S1(int StrategieS1, int StrategieS2, int StrategieS3) {
        //bedeuted Ausführug eines Zuges eine Spielers
        //Prinzp: switch-schleife mit Möglichkeiten für 8 Strategien: in den cases jeweils if-Abfragung ob spieler Trumpf hat, und dementsprechend einen Zug ausführen und Mitte ändern
        Listenelement temp = null;
        spieler1.setzeAusgesetzt(false);
        switch(StrategieS1) {
            case 0:
            if(TrumpfCheckS1() == false) {
                ZugS1();
            }
            else {
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);                                     

            }
            break;
            case 1:
            if(TrumpfCheckS1() == false) {
                ZugS1();
            }
            else{

                Listenelement v = spieler1.Strategie1();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS1(); 
                }
            }

            break;

            case 2:
            if(TrumpfCheckS1() == false) {
                ZugS1();
            }
            else{
                Listenelement v = spieler1.Strategie2( );
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS1(); 
                }
            }

            break;

            case 3:
            if(TrumpfCheckS1() == false) {


                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler1.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        if(spieler1.gebeAusgesetzt() == false) {
                            
                        ZugS1();
                    }
                    
                    }
                }
                else{
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 4:
            if(TrumpfCheckS1() == false) {
                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler1.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        System.out.println("spieler1 setzt aus");
                    }
                }
                else{
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 5:
            if(TrumpfCheckS1() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler1.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                        spieler1.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    }
                    else {
                        ZugS1();
                    }

                }
                else{
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }

            break;

            case 6:
            if(TrumpfCheckS1() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler1.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                        Listenelement tem = spieler1.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                        if(temp != null) {
                            mitte.setzeMitte(2, tem.gebeZahl());
                        }
                        else {
                            spieler1.setzeAusgesetzt(true);
                            System.out.println("spieler1 setzt aus");
                        }
                    }
                    else {
                        ZugS1();
                    }
                }
                else {
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 7:
            if(TrumpfCheckS1() == false) {
                if(spieler1.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler1.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS1();
                    }
                }
                else{
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 8:
            if(TrumpfCheckS1() == false) {
                if(spieler1.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler1.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS1();
                    }
                }
                else {
                    ZugS1();
                }
            }
            else{
                Ausspielen4S1(StrategieS1, StrategieS2, StrategieS3);
            }
            break;
        }
    }

    public void Zug4S2(int StrategieS1, int StrategieS2, int StrategieS3) {
        //bedeuted Ausführug eines Zuges eine Spielers
        //Prinzp: switch-schleife mit Möglichkeiten für 8 Strategien: in den cases jeweils if-Abfragung ob spieler Trumpf hat, und dementsprechend einen Zug ausführen und Mitte ändern
        Listenelement temp = null;
        spieler2.setzeAusgesetzt(false);
        switch(StrategieS2) {
            case 0:
            if(TrumpfCheckS2() == false) {
                ZugS2();
            }
            else {
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);                                     

            }
            break;
            case 1:
            if(TrumpfCheckS2() == false) {
                ZugS2();
            }
            else{

                Listenelement v = spieler2.Strategie1();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS2(); 
                }
            }

            break;

            case 2:
            if(TrumpfCheckS2() == false) {
                ZugS2();
            }
            else{
                Listenelement v = spieler2.Strategie2();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS2(); 
                }
            }

            break;

            case 3:
            if(TrumpfCheckS2() == false) {


                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler2.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        if(spieler2.gebeAusgesetzt() == false) {
                            ZugS2();
                        }

                    }
                }
                else{
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 4:
            if(TrumpfCheckS2() == false) {
                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler2.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        System.out.println("spieler2 setzt aus");
                    }
                }
                else{
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 5:
            if(TrumpfCheckS2() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler2.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                        spieler2.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    }
                    else {
                        ZugS2();
                    }

                }
                else{
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }

            break;

            case 6:
            if(TrumpfCheckS2() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler2.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                        Listenelement tem = spieler2.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                        if(temp != null) {
                            mitte.setzeMitte(2, tem.gebeZahl());
                        }
                        else {
                            spieler2.setzeAusgesetzt(true);
                            System.out.println("spieler2 setzt aus");
                        }
                    }
                    else {
                        ZugS2();
                    }
                }
                else {
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 7:
            if(TrumpfCheckS2() == false) {
                if(spieler2.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler2.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS2();
                    }
                }
                else{
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }

            break;
            case 8:
            if(TrumpfCheckS2() == false) {
                if(spieler2.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler2.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS2();
                    }
                }
                else {
                    ZugS2();
                }
            }
            else{
                Ausspielen4S2(StrategieS1, StrategieS2, StrategieS3);
            }
            break;
        }
    }

    public void Zug4S3(int StrategieS1, int StrategieS2, int StrategieS3) {
        //bedeuted Ausführug eines Zuges eine Spielers
        //Prinzp: switch-schleife mit Möglichkeiten für 8 Strategien: in den cases jeweils if-Abfragung ob spieler Trumpf hat, und dementsprechend einen Zug ausführen und Mitte ändern
        Listenelement temp = null;
        spieler3.setzeAusgesetzt(false);
        switch(StrategieS3) {
            case 0:
            if(TrumpfCheckS3() == false) {
                ZugS3();
            }
            else {
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);                                     

            }
            break;
            case 1:
            if(TrumpfCheckS3() == false) {
                ZugS3();
            }
            else{

                Listenelement v = spieler3.Strategie1();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS3(); 
                }
            }

            break;

            case 2:
            if(TrumpfCheckS3() == false) {
                ZugS3();
            }
            else{
                Listenelement v = spieler3.Strategie2( );
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS3(); 
                }
            }

            break;

            case 3:
            if(TrumpfCheckS3() == false) {


                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler3.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        if(spieler3.gebeAusgesetzt() == false) {
                            ZugS3();
                        }


                    }
                }
                else{
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }

            break;
            case 4:
            if(TrumpfCheckS3() == false) {
                if(mitte.gebeMehrlinge() == 1) {
                    temp = spieler3.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(1,temp.gebeZahl());
                    }
                    else{
                        System.out.println("spieler3 setzt aus");
                    }
                }
                else{
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }

            break;
            case 5:
            if(TrumpfCheckS3() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler3.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                        spieler3.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    }
                    else {
                        ZugS3();
                    }

                }
                else{
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }

            break;

            case 6:
            if(TrumpfCheckS3() == false) {
                if(mitte.gebeMehrlinge() == 2) {
                    if(spieler3.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                        Listenelement tem = spieler3.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                        if(temp != null) {
                            mitte.setzeMitte(2, tem.gebeZahl());
                        }
                        else {
                            spieler3.setzeAusgesetzt(true);
                            System.out.println("spieler3 setzt aus");
                        }
                    }
                    else {
                        ZugS3();
                    }
                }
                else {
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }

            break;
            case 7:
            if(TrumpfCheckS3() == false) {
                if(spieler3.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler3.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS3();
                    }
                }
                else{
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }

            break;
            case 8:
            if(TrumpfCheckS3() == false) {
                if(spieler3.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement x = spieler3.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(x != null) {
                        mitte.setzeMitte(1,x.gebeZahl());
                    }
                    else{
                        ZugS3();
                    }
                }
                else {
                    ZugS3();
                }
            }
            else{
                Ausspielen4S3(StrategieS1, StrategieS3, StrategieS3);
            }
            break;
        }
    }

    public void Ausspielen4S1(int StrategieS1, int StrategieS2,int StrategieS3) {
        //bedeuted Ausspielen eine Spielers
        //Prinzp: if-Schleife, welche Strategie der Spieler verfolgt, Ausführung des Zuges, Ändern der Mitte
        if(StrategieS1 == 1) {
            Listenelement v = spieler1.Strategie1();
            if(v != null) {
                MitteSetzen(1,v.gebeZahl());
            }
            else{
                AusspielenS1();
            }
        }

        else { if(StrategieS1 == 2) {
                Listenelement v = spieler1.Strategie2();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS1();
                }
            }

            else{
                AusspielenS1();
            }
        }
    }

    public void Ausspielen4S2(int StrategieS1, int StrategieS2,int StrategieS3) {
         //bedeuted Ausspielen eine Spielers
        //Prinzp: if-Schleife, welche Strategie der Spieler verfolgt, Ausführung des Zuges, Ändern der Mitte
        if(StrategieS2 == 1) {
            Listenelement v = spieler2.Strategie1();
            if(v != null) {
                MitteSetzen(1,v.gebeZahl());
            }
            else{
                AusspielenS2();
            }
        }

        else { if(StrategieS2 == 2) {
                Listenelement v = spieler2.Strategie2();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS2();
                }
            }

            else{
                AusspielenS2();
            }
        }
    }

    public void Ausspielen4S3(int StrategieS1, int StrategieS2,int StrategieS3) {
         //bedeuted Ausspielen eine Spielers
        //Prinzp: if-Schleife, welche Strategie der Spieler verfolgt, Ausführung des Zuges, Ändern der Mitte
        if(StrategieS3 == 1) {
            Listenelement v = spieler3.Strategie1();
            if(v != null) {
                MitteSetzen(1,v.gebeZahl());
            }
            else{
                AusspielenS3();
            }
        }

        else { if(StrategieS3 == 2) {
                Listenelement v = spieler3.Strategie2();
                if(v != null) {
                    MitteSetzen(2,v.gebeZahl());
                }
                else{
                    AusspielenS3();
                }
            }

            else{
                AusspielenS3();
            }
        }
    }

    public void AusspielenS1_2() { //Ausspielen eines Spielers mit Strategie 2
        Listenelement v = spieler1.Strategie2();
        if(v != null) {
            MitteSetzen(2,v.gebeZahl());
        }
        else{
            ZugS1();
        }

    }

    public void AusspielenS2_2() { //Ausspielen eines Spielers mit Strategie 2
        Listenelement v = spieler2.Strategie2();
        if(v != null) {
            MitteSetzen(2,v.gebeZahl());
        }
        else{
            ZugS2();
        }

    }

    public void AusspielenS3_2() {  //Ausspielen eines Spielers mit Strategie 2
        Listenelement v = spieler3.Strategie2();
        if(v != null) {
            MitteSetzen(2,v.gebeZahl());
        }
        else{
            ZugS3();
        }

    }

    public void ZugS1() {
        //Zug eines Spielers mit Dummy-Strategie
        //if-Abfrage ob Spieler Trumpf hat: Mitte entsprechend nach Legen4() ändern oder ausspieln
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        // System.out.println("Spieler 1");
        //s = spieler1.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(TrumpfCheckS1() == false) {
            //System.out.println("                    debug_TrumpfCheckS1() == false");
            int zahlMitte = mitte.gebeZahl();
            //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
            spieler1.setzeS(null);
            z = spieler1.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
            if(spieler1.gebeAusgesetzt() == false) {  //|| */z  !=  null  

                //System.out.println("                    debug_z != null");
                //mitte.setzeZahl(z.gebeZahl());
                //System.out.println("                    debug_Aufruf Legen4 in Spiel");
                spieler1.Legen4(mitte.gebeMehrlinge(), zahlMitte);

                mitte.setzeMehrlinge(spieler1.gebeMehrlingsSpeicher());
                mitte.setzeZahl(spieler1.gebeZahlSpeicher());
                MitteOutprint();
            }

            else {
                //System.out.println("                    debug_z == null");
                System.out.println("spieler1 setzt aus");
                spieler1.setzeAusgesetzt(true);
            }
        }


        else{ //System.out.println("                    debug_TrumpfCheckS1() == true");
            if(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl()) != 4) {
                if(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl()) > 1) {
                    mitte.setzeZahl(spieler1.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler1 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler1 legt: " + e3f + "_" +e3z);
                        spieler1.entferne(e3f , e3z);
                        spieler1.entferne(e2f,e2z );
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler1 legt: " + e2f + "_" +e2z);

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler1.entferne(e2f,e2z );
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
                else{

                    if(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl()) == 1) {
                        spieler1.entferne2(spieler1.gebeKleinstesBlatt());
                        System.out.println("spieler1 legt: " + spieler1.gebeKleinstesBlatt().gebeFarbe() + "_" + spieler1.gebeKleinstesBlatt().gebeZahl());
                        mitte.setzeMitte(1, spieler1.gebeKleinstesBlatt().gebeZahl());
                    }
                    else {
                        if(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl() + 1) > 1) {
                            Listenelement p = spieler1.gebeKleinstesBlattPlusX(1);
                            mitte.setzeMehrlinge(spieler1.MehrlingsCheck2(p.gebeZahl()));
                            mitte.setzeZahl(p.gebeZahl());

                            //Listenelement p = spieler1.WipeOutNumber(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                            System.out.println("spieler1 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                            switch(spieler1.MehrlingsCheck2(p.gebeZahl())) {

                                case 4:
                                // System.out.println("IIId");

                                spieler1.setzeBombe(p.gebeZahl()) ;

                                break;
                                case 3:
                                System.out.println("IIIc");

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                System.out.println(spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                spieler1.entferne2(p.gebeNachfolger().gebeNachfolger());
                                spieler1.entferne2(p.gebeNachfolger());
                                spieler1.entferne2(p);
                                break;
                                case 2:;
                                System.out.println("IIIb");

                                System.out.println((spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                spieler1.entferne2(p.gebeNachfolger());
                                spieler1.entferne2(p);
                                break;
                                case 1: 
                                System.out.println("IIIa");

                                System.out.println(spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                spieler1.entferne2(p);
                                break;
                            }

                        }
                        else {
                            if(spieler1.gebeKleinstesBlatt().gebeZahl() + 2 > 1) {
                                Listenelement p = spieler1.gebeKleinstesBlattPlusX(2);
                                mitte.setzeMehrlinge(spieler1.MehrlingsCheck2(p.gebeZahl()));
                                mitte.setzeZahl(p.gebeZahl());

                                //Listenelement p = spieler1.WipeOutNumber(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                                System.out.println("spieler1 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                                switch(spieler1.MehrlingsCheck2(p.gebeZahl())) {

                                    case 4:
                                    // System.out.println("IIId");

                                    spieler1.setzeBombe(p.gebeZahl()) ;
                                    System.out.println("spieler1 hat Bombe");

                                    break;
                                    case 3:
                                    System.out.println("IIIc");

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    System.out.println(spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                    System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                    spieler1.entferne2(p.gebeNachfolger().gebeNachfolger());
                                    spieler1.entferne2(p.gebeNachfolger());
                                    spieler1.entferne2(p);
                                    break;
                                    case 2:;
                                    System.out.println("IIIb");

                                    System.out.println((spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                    System.out.println(spieler1 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                    spieler1.entferne2(p.gebeNachfolger());
                                    spieler1.entferne2(p);
                                    break;
                                    case 1: 
                                    System.out.println("IIIa");

                                    System.out.println(spieler1 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    spieler1.entferne2(p);
                                    break;
                                }
                            }
                            else {
                                mitte.setzeZahl(spieler1.gebeKleinstesBlatt().gebeZahl());
                                switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                                    case 4:

                                    break;
                                    case 3:
                                    e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                                    e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                                    e3f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                                    e3z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                                    break;

                                    case 2:

                                    //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                                    e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                                    e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                                    //}
                                    break;

                                    case 1:

                                    //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                                    e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                                    //}
                                    break;
                                }
                                switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                                    case 3:
                                    System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler1 legt: " + e2f + "_" +e2z);
                                    System.out.println("spieler1 legt: " + e3f + "_" +e3z);
                                    spieler1.entferne(e3f , e3z);
                                    spieler1.entferne(e2f,e2z );
                                    spieler1.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(3);
                                    break;

                                    case 2:
                                    System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler1 legt: " + e2f + "_" +e2z);

                                    //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                                    spieler1.entferne(e2f,e2z );
                                    spieler1.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(2);
                                    //}
                                    break;

                                    case 1:
                                    System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                                    spieler1.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(1);

                                    //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                                    //}
                                    break;
                                }
                            }

                        }
                    }
                }

                //MitteOutprint();
                //System.out.print("Spieler 1: GEGE");

                MitteOutprint();

            }
            else { 
                if(spieler1.gebeAnzahlKarten() != 4) {
                    spieler1.entferne2(spieler1.BombeNichtAusspielen());;
                }
                else {
                    mitte.setzeZahl(spieler1.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler1 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler1 legt: " + e3f + "_" +e3z);
                        spieler1.entferne(e3f , e3z);
                        spieler1.entferne(e2f,e2z );
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler1 legt: " + e2f + "_" +e2z);

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler1.entferne(e2f,e2z );
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler1 legt: " + e1f + "_" +e1z);
                        spieler1.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
            }
        }
    }

    public void ZugS2() {
        //Zug eines Spielers mit Dummy-Strategie
        //if-Abfrage ob Spieler Trumpf hat: Mitte entsprechend nach Legen4() ändern oder ausspieln
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        // System.out.println("Spieler 1");
        //s = spieler2.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(TrumpfCheckS2() == false) {
            //System.out.println("                    debug_TrumpfCheckS2() == false");
            int zahlMitte = mitte.gebeZahl();
            //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
            spieler2.setzeS(null);
            z = spieler2.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
            if(spieler2.gebeAusgesetzt() == false) {  //|| */z  !=  null  

                //System.out.println("                    debug_z != null");
                //mitte.setzeZahl(z.gebeZahl());
                //System.out.println("                    debug_Aufruf Legen4 in Spiel");
                spieler2.Legen4(mitte.gebeMehrlinge(), zahlMitte);

                mitte.setzeMehrlinge(spieler2.gebeMehrlingsSpeicher());
                mitte.setzeZahl(spieler2.gebeZahlSpeicher());
                MitteOutprint();
            }

            else {
                //System.out.println("                    debug_z == null");
                //System.out.println("                    debug_spieler2 setzt aus");
                spieler2.setzeAusgesetzt(true);
            }
        }

        
        else{ 
            //System.out.println("                    debug_TrumpfCheckS2() == true");
            if(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl()) != 4) {
                if(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl()) > 1) {
                    mitte.setzeZahl(spieler2.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler2 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler2 legt: " + e3f + "_" +e3z);
                        spieler2.entferne(e3f , e3z);
                        spieler2.entferne(e2f,e2z );
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler2 legt: " + e2f + "_" +e2z);

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler2.entferne(e2f,e2z );
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
                else{

                    if(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl()) == 1) {
                        spieler2.entferne2(spieler2.gebeKleinstesBlatt());
                        System.out.println("spieler2 legt: " + spieler2.gebeKleinstesBlatt().gebeFarbe() + "_" + spieler2.gebeKleinstesBlatt().gebeZahl());
                        mitte.setzeMitte(1, spieler2.gebeKleinstesBlatt().gebeZahl());
                    }
                    else {
                        if(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl() + 1) > 1) {
                            Listenelement p = spieler2.gebeKleinstesBlattPlusX(1);
                            mitte.setzeMehrlinge(spieler2.MehrlingsCheck2(p.gebeZahl()));
                            mitte.setzeZahl(p.gebeZahl());

                            //Listenelement p = spieler2.WipeOutNumber(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                            System.out.println("spieler2 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                            switch(spieler2.MehrlingsCheck2(p.gebeZahl())) {

                                case 4:
                                // System.out.println("IIId");

                                spieler2.setzeBombe(p.gebeZahl()) ;

                                break;
                                case 3:
                                System.out.println("IIIc");

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                System.out.println(spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                spieler2.entferne2(p.gebeNachfolger().gebeNachfolger());
                                spieler2.entferne2(p.gebeNachfolger());
                                spieler2.entferne2(p);
                                break;
                                case 2:;
                                System.out.println("IIIb");

                                System.out.println((spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                spieler2.entferne2(p.gebeNachfolger());
                                spieler2.entferne2(p);
                                break;
                                case 1: 
                                System.out.println("IIIa");

                                System.out.println(spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                spieler2.entferne2(p);
                                break;
                            }

                        }
                        else {
                            if(spieler2.gebeKleinstesBlatt().gebeZahl() + 2 > 1) {
                                Listenelement p = spieler2.gebeKleinstesBlattPlusX(2);
                                mitte.setzeMehrlinge(spieler2.MehrlingsCheck2(p.gebeZahl()));
                                mitte.setzeZahl(p.gebeZahl());

                                //Listenelement p = spieler2.WipeOutNumber(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                                System.out.println("spieler2 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                                switch(spieler2.MehrlingsCheck2(p.gebeZahl())) {

                                    case 4:
                                    // System.out.println("IIId");

                                    spieler2.setzeBombe(p.gebeZahl()) ;
                                    System.out.println("spieler2 hat Bombe");

                                    break;
                                    case 3:
                                    System.out.println("IIIc");

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    System.out.println(spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                    System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                    spieler2.entferne2(p.gebeNachfolger().gebeNachfolger());
                                    spieler2.entferne2(p.gebeNachfolger());
                                    spieler2.entferne2(p);
                                    break;
                                    case 2:;
                                    System.out.println("IIIb");

                                    System.out.println((spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                    System.out.println(spieler2 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                    spieler2.entferne2(p.gebeNachfolger());
                                    spieler2.entferne2(p);
                                    break;
                                    case 1: 
                                    System.out.println("IIIa");

                                    System.out.println(spieler2 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    spieler2.entferne2(p);
                                    break;
                                }
                            }
                            else {
                                mitte.setzeZahl(spieler2.gebeKleinstesBlatt().gebeZahl());
                                switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                                    case 4:

                                    break;
                                    case 3:
                                    e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                                    e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                                    e3f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                                    e3z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                                    break;

                                    case 2:

                                    //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                                    e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                                    e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                                    //}
                                    break;

                                    case 1:

                                    //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                                    e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                                    //}
                                    break;
                                }
                                switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                                    case 3:
                                    System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler2 legt: " + e2f + "_" +e2z);
                                    System.out.println("spieler2 legt: " + e3f + "_" +e3z);
                                    spieler2.entferne(e3f , e3z);
                                    spieler2.entferne(e2f,e2z );
                                    spieler2.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(3);
                                    break;

                                    case 2:
                                    System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler2 legt: " + e2f + "_" +e2z);

                                    //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                                    spieler2.entferne(e2f,e2z );
                                    spieler2.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(2);
                                    //}
                                    break;

                                    case 1:
                                    System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                                    spieler2.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(1);

                                    //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                                    //}
                                    break;
                                }
                            }

                        }
                    }
                }

                //MitteOutprint();
                //System.out.print("Spieler 1: GEGE");

                MitteOutprint();

            }
            else { 
                if(spieler2.gebeAnzahlKarten() != 4) {
                    spieler2.entferne2(spieler2.BombeNichtAusspielen());;
                }
                else {
                    mitte.setzeZahl(spieler2.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler2 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler2 legt: " + e3f + "_" +e3z);
                        spieler2.entferne(e3f , e3z);
                        spieler2.entferne(e2f,e2z );
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler2 legt: " + e2f + "_" +e2z);

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler2.entferne(e2f,e2z );
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler2 legt: " + e1f + "_" +e1z);
                        spieler2.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
            }
        }
    }

    public void ZugS3() {
        //Zug eines Spielers mit Dummy-Strategie
        //if-Abfrage ob Spieler Trumpf hat: Mitte entsprechend nach Legen4() ändern oder ausspieln
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        // System.out.println("Spieler 1");
        //s = spieler3.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(TrumpfCheckS3() == false) {
            //System.out.println("                    debug_TrumpfCheckS3() == false");
            int zahlMitte = mitte.gebeZahl();
            //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
            spieler3.setzeS(null);
            z = spieler3.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
            if(spieler3.gebeAusgesetzt() == false) {  //|| */z  !=  null  

                //System.out.println("                    debug_z != null");
                //mitte.setzeZahl(z.gebeZahl());
                //System.out.println("                    debug_Aufruf Legen4 in Spiel");
                spieler3.Legen4(mitte.gebeMehrlinge(), zahlMitte);

                mitte.setzeMehrlinge(spieler3.gebeMehrlingsSpeicher());
                mitte.setzeZahl(spieler3.gebeZahlSpeicher());
                MitteOutprint();
            }

            else {
                //System.out.println("                    debug_z == null");
                System.out.println("spieler3 setzt aus");
                spieler3.setzeAusgesetzt(true);
            }
        }


        else{ 
            //System.out.println("                    debug_TrumpfCheckS3() == true");
            if(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl()) != 4) {
                if(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl()) > 1) {
                    mitte.setzeZahl(spieler3.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler3 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler3 legt: " + e3f + "_" +e3z);
                        spieler3.entferne(e3f , e3z);
                        spieler3.entferne(e2f,e2z );
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler3 legt: " + e2f + "_" +e2z);

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler3.entferne(e2f,e2z );
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
                else{

                    if(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl()) == 1) {
                        spieler3.entferne2(spieler3.gebeKleinstesBlatt());
                        System.out.println("spieler3 legt: " + spieler3.gebeKleinstesBlatt().gebeFarbe() + "_" + spieler3.gebeKleinstesBlatt().gebeZahl());
                        mitte.setzeMitte(1, spieler3.gebeKleinstesBlatt().gebeZahl());
                    }
                    else {
                        if(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl() + 1) > 1) {
                            Listenelement p = spieler3.gebeKleinstesBlattPlusX(1);
                            mitte.setzeMehrlinge(spieler3.MehrlingsCheck2(p.gebeZahl()));
                            mitte.setzeZahl(p.gebeZahl());

                            //Listenelement p = spieler3.WipeOutNumber(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                            System.out.println("spieler3 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                            switch(spieler3.MehrlingsCheck2(p.gebeZahl())) {

                                case 4:
                                // System.out.println("IIId");

                                spieler3.setzeBombe(p.gebeZahl()) ;

                                break;
                                case 3:
                                System.out.println("IIIc");

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                System.out.println(spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                spieler3.entferne2(p.gebeNachfolger().gebeNachfolger());
                                spieler3.entferne2(p.gebeNachfolger());
                                spieler3.entferne2(p);
                                break;
                                case 2:;
                                System.out.println("IIIb");

                                System.out.println((spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                spieler3.entferne2(p.gebeNachfolger());
                                spieler3.entferne2(p);
                                break;
                                case 1: 
                                System.out.println("IIIa");

                                System.out.println(spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                spieler3.entferne2(p);
                                break;
                            }

                        }
                        else {
                            if(spieler3.gebeKleinstesBlatt().gebeZahl() + 2 > 1) {
                                Listenelement p = spieler3.gebeKleinstesBlattPlusX(2);
                                mitte.setzeMehrlinge(spieler3.MehrlingsCheck2(p.gebeZahl()));
                                mitte.setzeZahl(p.gebeZahl());

                                //Listenelement p = spieler3.WipeOutNumber(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl() + 1)) ;
                                System.out.println("spieler3 legt: " + p.gebeFarbe() + "_" + p.gebeZahl());
                                switch(spieler3.MehrlingsCheck2(p.gebeZahl())) {

                                    case 4:
                                    // System.out.println("IIId");

                                    spieler3.setzeBombe(p.gebeZahl()) ;
                                    System.out.println("spieler3 hat Bombe");

                                    break;
                                    case 3:
                                    System.out.println("IIIc");

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    System.out.println(spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));
                                    System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeNachfolger().gebeZahl()));

                                    spieler3.entferne2(p.gebeNachfolger().gebeNachfolger());
                                    spieler3.entferne2(p.gebeNachfolger());
                                    spieler3.entferne2(p);
                                    break;
                                    case 2:;
                                    System.out.println("IIIb");

                                    System.out.println((spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl())));
                                    System.out.println(spieler3 + " legt: " + p.gebeNachfolger().gebeFarbe() + "_" + ( p.gebeNachfolger().gebeZahl()));

                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);

                                    spieler3.entferne2(p.gebeNachfolger());
                                    spieler3.entferne2(p);
                                    break;
                                    case 1: 
                                    System.out.println("IIIa");

                                    System.out.println(spieler3 + " legt: " + p.gebeFarbe() + "_" + ( p.gebeZahl()));
                                    //s = kleinstesBlatt.Legen4(mehrlingeMitte,zahlMitte);
                                    spieler3.entferne2(p);
                                    break;
                                }
                            }
                            else {
                                mitte.setzeZahl(spieler3.gebeKleinstesBlatt().gebeZahl());
                                switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                                    case 4:

                                    break;
                                    case 3:
                                    e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                                    e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                                    e3f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                                    e3z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                                    break;

                                    case 2:

                                    //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                                    e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                                    e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                                    e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                                    //}
                                    break;

                                    case 1:

                                    //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                                    e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                                    e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                                    //}
                                    break;
                                }
                                switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                                    case 3:
                                    System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler3 legt: " + e2f + "_" +e2z);
                                    System.out.println("spieler3 legt: " + e3f + "_" +e3z);
                                    spieler3.entferne(e3f , e3z);
                                    spieler3.entferne(e2f,e2z );
                                    spieler3.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(3);
                                    break;

                                    case 2:
                                    System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                                    System.out.println("spieler3 legt: " + e2f + "_" +e2z);

                                    //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                                    spieler3.entferne(e2f,e2z );
                                    spieler3.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(2);
                                    //}
                                    break;

                                    case 1:
                                    System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                                    spieler3.entferne(e1f, e1z );
                                    mitte.setzeMehrlinge(1);

                                    //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                                    //}
                                    break;
                                }
                            }

                        }
                    }
                }

                //MitteOutprint();
                //System.out.print("Spieler 1: GEGE");

                MitteOutprint();

            }
            else { 
                if(spieler3.gebeAnzahlKarten() != 4) {
                    spieler3.entferne2(spieler3.BombeNichtAusspielen());;
                }
                else {
                    mitte.setzeZahl(spieler3.gebeKleinstesBlatt().gebeZahl());
                    switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                        case 4:

                        break;
                        case 3:
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                        e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                        e3f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                        e3z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                        break;

                        case 2:

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
                        e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

                        //}
                        break;

                        case 1:

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
                        e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                        e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                        //}
                        break;
                    }
                    switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

                        case 3:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler3 legt: " + e2f + "_" +e2z);
                        System.out.println("spieler3 legt: " + e3f + "_" +e3z);
                        spieler3.entferne(e3f , e3z);
                        spieler3.entferne(e2f,e2z );
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(3);
                        break;

                        case 2:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        System.out.println("spieler3 legt: " + e2f + "_" +e2z);

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

                        spieler3.entferne(e2f,e2z );
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(2);
                        //}
                        break;

                        case 1:
                        System.out.println("spieler3 legt: " + e1f + "_" +e1z);
                        spieler3.entferne(e1f, e1z );
                        mitte.setzeMehrlinge(1);

                        //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

                        //}
                        break;
                    }

                }
            }
        }
    }

    public void DraufLegen4S1(int StrategieS1, int StrategieS2, int StrategieS3) {
        //Methode, um einen Spieler zum l egenauf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat
        //switch-Schleife, welche bei jeder Strategie jeweils gemäß der Strategie drauflegt und die Mitte ändert
        switch(StrategieS1) {
            case 0:
            DraufLegenS1();
            break;

            case 1:

            DraufLegenS1();
            break;
            case 2:
            DraufLegenS1();
            break;
            case 3:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler1.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
if(spieler1.gebeAusgesetzt() == false) {
                            ZugS1();
                        }

                }
            }
            else{
                ZugS1();
            }

            break;

            case 4:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler1.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
                    spieler1.setzeAusgesetzt(true);
                    System.out.println("spieler1 setzt aus");
                }
            }
            else{
                ZugS1();
            }

            break;
            case 5:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler1.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                    Listenelement temp = spieler1.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler1.setzeAusgesetzt(true);
                        System.out.println("spieler1 setzt aus");
                    }
                }
                else {
                    ZugS1();
                }
            }
            else{
                ZugS1();
            }

            break;

            case 6:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler1.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement temp = spieler1.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler1.setzeAusgesetzt(true);
                        System.out.println("spieler1 setzt aus");
                    }

                }
                else {
                    ZugS1();
                }
            }
            else {
                ZugS1();
            }

            break;
            case 7:
            if(spieler1.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler1.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler1.setzeAusgesetzt(true);
                    System.out.println("spieler1 setzt aus");
                }

            }
            else {
                ZugS1();
            }
            break;
            case 8: 
            if(spieler1.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler1.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler1.setzeAusgesetzt(true);
                    System.out.println("spieler1 setzt aus");
                }

            }
            else {
                ZugS1();
            }
            break;
        }
    }

    public void DraufLegen4S2(int StrategieS1, int StrategieS2, int StrategieS3) {
        //Methode, um einen Spieler zum l egenauf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat
        //switch-Schleife, welche bei jeder Strategie jeweils gemäß der Strategie drauflegt und die Mitte ändert
        switch(StrategieS2) {
            case 0:
            DraufLegenS2();
            break;

            case 1:

            DraufLegenS2();
            break;
            case 2:
            DraufLegenS2();
            break;
            case 3:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler2.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
if(spieler2.gebeAusgesetzt() == false) {
                            ZugS2();
                        }

                }
            }
            else{
                ZugS2();
            }

            break;

            case 4:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler2.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
                    spieler2.setzeAusgesetzt(true);
                    System.out.println("spieler2 setzt aus");
                }
            }
            else{
                ZugS2();
            }

            break;
            case 5:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler2.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                    Listenelement temp = spieler2.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler2.setzeAusgesetzt(true);
                        System.out.println("spieler2 setzt aus");
                    }
                }
                else {
                    ZugS2();
                }
            }
            else{
                ZugS2();
            }

            break;

            case 6:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler2.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement temp = spieler2.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler2.setzeAusgesetzt(true);
                        System.out.println("spieler2 setzt aus");
                    }

                }
                else {
                    ZugS2();
                }
            }
            else {
                ZugS2();
            }

            break;
            case 7:
            if(spieler2.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler2.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler2.setzeAusgesetzt(true);
                    System.out.println("spieler2 setzt aus");
                }

            }
            else {
                ZugS2();
            }
            break;
            case 8: 
            if(spieler2.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler2.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler2.setzeAusgesetzt(true);
                    System.out.println("spieler2 setzt aus");
                }

            }
            else {
                ZugS2();
            }
            break;
        }
    }

    public void DraufLegen4S3(int StrategieS1, int StrategieS2, int StrategieS3) {
        //Methode, um einen Spieler zum l egenauf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat
        //switch-Schleife, welche bei jeder Strategie jeweils gemäß der Strategie drauflegt und die Mitte ändert
        switch(StrategieS3) {
            case 0:
            DraufLegenS3();
            break;

            case 1:

            DraufLegenS3();
            break;
            case 2:
            DraufLegenS3();
            break;
            case 3:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler3.FallStrat3Keep(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
                                       if(spieler3.gebeAusgesetzt() == false) {
                            ZugS3();
                        }

                }
            }
            else{
                ZugS3();
            }

            break;

            case 4:
            if(mitte.gebeMehrlinge() == 1) {
                Listenelement temp = spieler3.Strategie4(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(temp != null) {
                    mitte.setzeMitte(1,temp.gebeZahl());
                }
                else{
                    spieler3.setzeAusgesetzt(true);
                    System.out.println("spieler3 setzt aus");
                }
            }
            else{
                ZugS3();
            }

            break;
            case 5:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler3.FallStrat5HighDoubleKeep(mitte.gebeMehrlinge(), mitte.gebeZahl()) == true) {
                    Listenelement temp = spieler3.Strategie5(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler3.setzeAusgesetzt(true);
                        System.out.println("spieler3 setzt aus");
                    }
                }
                else {
                    ZugS3();
                }
            }
            else{
                ZugS3();
            }

            break;

            case 6:
            if(mitte.gebeMehrlinge() == 2) {
                if(spieler3.FallStrat6HighDoublePlay(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                    Listenelement temp = spieler3.Strategie6(mitte.gebeMehrlinge(), mitte.gebeZahl());
                    if(temp != null) {
                        mitte.setzeMitte(2,temp.gebeZahl());
                    }
                    else{
                        spieler3.setzeAusgesetzt(true);
                        System.out.println("spieler3 setzt aus");
                    }

                }
                else {
                    ZugS3();
                }
            }
            else {
                ZugS3();
            }

            break;
            case 7:
            if(spieler3.FallStrat7(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler3.Strategie7(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler3.setzeAusgesetzt(true);
                    System.out.println("spieler3 setzt aus");
                }

            }
            else {
                ZugS3();
            }
            break;
            case 8: 
            if(spieler3.FallStrat8(mitte.gebeMehrlinge(), mitte.gebeZahl()) != null) {
                Listenelement x = spieler3.Strategie8(mitte.gebeMehrlinge(), mitte.gebeZahl());
                if(x != null) {
                    mitte.setzeMitte(1,x.gebeZahl());
                }
                else{
                    spieler3.setzeAusgesetzt(true);
                    System.out.println("spieler3 setzt aus");
                }

            }
            else {
                ZugS3();
            }
            break;
        }
    }

    public void DraufLegenS1() {
        //Methode, um einen Spieler mit Dummy-Strategie zum legen auf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat)
        //if-Abfrage ob ausgesetzt true ist
        spieler1.setzeAusgesetzt(false);
        //System.out.println("                    debug_spieler1 ist am Zuge");
        Listenelement s;
        Listenelement z = null;

        int zahlMitte = mitte.gebeZahl();
        //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
        spieler1.setzeS(null);
        z = spieler1.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(spieler1.gebeAusgesetzt() == false) {  //|| */z  !=  null  

            //System.out.println("                    debug_z != null");
            //mitte.setzeZahl(z.gebeZahl());
            //System.out.println("                    debug_Aufruf Legen4 in Spiel");
            spieler1.Legen4(mitte.gebeMehrlinge(), zahlMitte);

            mitte.setzeMehrlinge(spieler1.gebeMehrlingsSpeicher());
            mitte.setzeZahl(spieler1.gebeZahlSpeicher());
            MitteOutprint();
        }

        else {
            //System.out.println("                    debug_z == null");
            System.out.println("spieler1 setzt aus");
            spieler1.setzeAusgesetzt(true);
        }
    }

    public void DraufLegenS2() {
        //Methode, um einen Spieler mit Dummy-Strategie zum legen auf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat)
        //if-Abfrage ob ausgesetzt true ist
        spieler2.setzeAusgesetzt(false);
        //System.out.println("                    debug_spieler2 ist am Zuge");
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;
        int zahlMitte = mitte.gebeZahl();
        //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
        spieler2.setzeS(null);
        z = spieler2.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(spieler2.gebeAusgesetzt() == false) {  //|| */z  !=  null  

            //System.out.println("                    debug_z != null");
            //mitte.setzeZahl(z.gebeZahl());
            //System.out.println("                    debug_Aufruf Legen4 in Spiel");

            spieler2.Legen4(mitte.gebeMehrlinge(), zahlMitte);

            mitte.setzeMehrlinge(spieler2.gebeMehrlingsSpeicher());
            mitte.setzeZahl(spieler2.gebeZahlSpeicher());
            MitteOutprint();
        }

        else {
            //System.out.println("                    debug_z == null");

            System.out.println("spieler2 setzt aus");
            spieler2.setzeAusgesetzt(true);
        }
    }

    public void DraufLegenS3() {
        //Methode, um einen Spieler mit Dummy-Strategie zum legen auf die Mitte zu bringen (wird aufgerufen im 1v1, wenn man keinen Trumpf hat)
        //if-Abfrage ob ausgesetzt true ist
        spieler3.setzeAusgesetzt(false);
        //System.out.println("                    debug_spieler3 ist am Zuge");
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;
        int zahlMitte = mitte.gebeZahl();
        //System.out.println("                    debug_Aufruf Legen4Passiv in Spiel");
        spieler3.setzeS(null);
        z = spieler3.Legen4Passiv(mitte.gebeMehrlinge(), mitte.gebeZahl());
        if(spieler3.gebeAusgesetzt() == false) {  //|| */z  !=  null  

            //System.out.println("                    debug_z != null");
            //mitte.setzeZahl(z.gebeZahl());
            //System.out.println("                    debug_Aufruf Legen4 in Spiel");
            spieler3.Legen4(mitte.gebeMehrlinge(), zahlMitte);

            mitte.setzeMehrlinge(spieler3.gebeMehrlingsSpeicher());
            mitte.setzeZahl(spieler3.gebeZahlSpeicher());
            MitteOutprint();
        }

        else {
            //System.out.println("                    debug_z == null");
            System.out.println("spieler3 setzt aus");
            spieler3.setzeAusgesetzt(true);
        }
    }

    public void AusspielenS1() {
        //Methode zum Ausspielen eines Spielers mit Dummy-Strategie im 1v1
        //switch-Schleife mit Überprüfung wie viele Mehrlinge man bei der niedrigsten Karte hat: dementsprechend entfernen und Mitte ändern
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        //System.out.println("                    debug_TrumpfCheckS1() == true");
        mitte.setzeZahl(spieler1.gebeKleinstesBlatt().gebeZahl());
        switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {

            case 4:
            if(spieler1.gebeAnzahlKarten() > 4) {
                //System.out.println("                    debug_spieler1 hat Bombe, aber spielt sie nicht aus");
                Listenelement w = spieler1.BombeNichtAusspielen();

                mitte.setzeMitte(spieler1.MehrlingsCheck2(w.gebeZahl()),w.gebeZahl()); 
                System.out.println("spieler 1 spielt: ");
                spieler1.WipeOutNumber2(w.gebeZahl());
            }
            else {
                //System.out.println("                    debug_spieler2 legt Bombe");
                e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
                e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
                e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                e3f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                e3z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();

            }
            break;
            case 3:
            e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
            e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
            e3f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
            e3z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
            break;

            case 2:

            //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
            e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
            e2z = spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

            //}
            break;

            case 1:

            //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
            e1f = spieler1.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler1.gebeKleinstesBlatt().gebeZahl();
            //}
            break;
        }
        switch(spieler1.MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeZahl())) {
            case 4:
            System.out.println("spieler1 legt: " + e1f + "_" +e1z);
            System.out.println("spieler1 legt: " + e2f + "_" +e2z);
            System.out.println("spieler1 legt: " + e3f + "_" +e3z);
            System.out.println("spieler1 legt: " + e4f + "_" +e4z);
            spieler1.entferne(e4f, e4z);
            spieler1.entferne(e3f , e3z);
            spieler1.entferne(e2f,e2z );
            spieler1.entferne(e1f, e1z );

            case 3:
            System.out.println("spieler1 legt: " + e1f + "_" +e1z);
            System.out.println("spieler1 legt: " + e2f + "_" +e2z);
            System.out.println("spieler1 legt: " + e3f + "_" +e3z);
            spieler1.entferne(e3f , e3z);
            spieler1.entferne(e2f,e2z );
            spieler1.entferne(e1f, e1z );
            mitte.setzeMehrlinge(3);
            break;

            case 2:
            System.out.println("spieler1 legt: " + e1f + "_" +e1z);
            System.out.println("spieler1 legt: " + e2f + "_" +e2z);

            //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

            spieler1.entferne(e2f,e2z );
            spieler1.entferne(e1f, e1z );
            mitte.setzeMehrlinge(2);
            //}
            break;

            case 1:
            System.out.println("spieler1 legt: " + e1f + "_" +e1z);
            spieler1.entferne(e1f, e1z );
            mitte.setzeMehrlinge(1);

            //if(spieler1.MehrlingsCheck2(spieler1.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

            //}
            break;
        }
    }

    public void AusspielenS2() {
        //Methode zum Ausspielen eines Spielers mit Dummy-Strategie im 1v1
        //switch-Schleife mit Überprüfung wie viele Mehrlinge man bei der niedrigsten Karte hat: dementsprechend entfernen und Mitte ändern
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        //System.out.println("                    debug_TrumpfCheckS2() == true");
        mitte.setzeZahl(spieler2.gebeKleinstesBlatt().gebeZahl());
        switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {

            case 4:
            if(spieler2.gebeAnzahlKarten() > 4) {
                //System.out.println("                    debug_spieler2 hat Bombe, aber spielt sie nicht aus");
                Listenelement w = spieler2.BombeNichtAusspielen();
                mitte.setzeMitte(spieler2.MehrlingsCheck2(w.gebeZahl()),w.gebeZahl()); 
                spieler2.WipeOutNumber2(w.gebeZahl());

            }
            else {
                //System.out.println("                    debug_spieler2 legt Bombe");
                e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
                e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
                e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                e3f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                e3z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();

            }
            break;
            case 3:
            e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
            e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
            e3f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
            e3z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
            break;

            case 2:

            //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
            e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
            e2z = spieler2.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

            //}
            break;

            case 1:

            //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
            e1f = spieler2.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler2.gebeKleinstesBlatt().gebeZahl();
            //}
            break;
        }
        switch(spieler2.MehrlingsCheck2(spieler2.gebeKleinstesBlatt().gebeZahl())) {
            case 4:
            System.out.println("spieler2 legt: " + e1f + "_" +e1z);
            System.out.println("spieler2 legt: " + e2f + "_" +e2z);
            System.out.println("spieler2 legt: " + e3f + "_" +e3z);
            System.out.println("spieler2 legt: " + e4f + "_" +e4z);
            spieler2.entferne(e4f, e4z);
            spieler2.entferne(e3f , e3z);
            spieler2.entferne(e2f,e2z );
            spieler2.entferne(e1f, e1z );

            case 3:
            System.out.println("spieler2 legt: " + e1f + "_" +e1z);
            System.out.println("spieler2 legt: " + e2f + "_" +e2z);
            System.out.println("spieler2 legt: " + e3f + "_" +e3z);
            spieler2.entferne(e3f , e3z);
            spieler2.entferne(e2f,e2z );
            spieler2.entferne(e1f, e1z );
            mitte.setzeMehrlinge(3);
            break;

            case 2:
            System.out.println("spieler2 legt: " + e1f + "_" +e1z);
            System.out.println("spieler2 legt: " + e2f + "_" +e2z);

            //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

            spieler2.entferne(e2f,e2z );
            spieler2.entferne(e1f, e1z );
            mitte.setzeMehrlinge(2);
            //}
            break;

            case 1:
            System.out.println("spieler2 legt: " + e1f + "_" +e1z);
            spieler2.entferne(e1f, e1z );
            mitte.setzeMehrlinge(1);

            //if(spieler2.MehrlingsCheck2(spieler2.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

            //}
            break;
        }
    }

    public void AusspielenS3() {
        //Methode zum Ausspielen eines Spielers mit Dummy-Strategie im 1v1
        //switch-Schleife mit Überprüfung wie viele Mehrlinge man bei der niedrigsten Karte hat: dementsprechend entfernen und Mitte ändern
        Listenelement s;
        Listenelement z = null;

        int e1f = -1;
        int e1z = -1;
        int e2f = -1;
        int e2z = -1;
        int e3f = -1;
        int e3z = -1;
        int e4f = -1;
        int e4z = -1;

        //System.out.println("                    debug_TrumpfCheckS3() == true");
        mitte.setzeZahl(spieler3.gebeKleinstesBlatt().gebeZahl());
        switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {

            case 4:
            if(spieler3.gebeAnzahlKarten() > 4) {
                //System.out.println("                    debug_spieler3 hat Bombe, aber spielt sie nicht aus");
                Listenelement w = spieler3.BombeNichtAusspielen();
                mitte.setzeMitte(spieler3.MehrlingsCheck2(w.gebeZahl()),w.gebeZahl()); 
                spieler3.WipeOutNumber2(w.gebeZahl());

            }
            else {
                //System.out.println("                    debug_spieler3 legt Bombe");
                e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
                e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
                e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
                e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
                e3f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
                e3z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();
                e4z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeNachfolger().gebeZahl();

            }
            break;
            case 3:
            e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe() ;
            e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();
            e3f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeFarbe() ;
            e3z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeNachfolger().gebeZahl();
            break;

            case 2:

            //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {
            e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
            e2f = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe();
            e2z = spieler3.gebeKleinstesBlatt().gebeNachfolger().gebeZahl();

            //}
            break;

            case 1:

            //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {
            e1f = spieler3.gebeKleinstesBlatt().gebeFarbe() ;
            e1z = spieler3.gebeKleinstesBlatt().gebeZahl();
            //}
            break;
        }
        switch(spieler3.MehrlingsCheck2(spieler3.gebeKleinstesBlatt().gebeZahl())) {
            case 4:
            System.out.println("spieler3 legt: " + e1f + "_" +e1z);
            System.out.println("spieler3 legt: " + e2f + "_" +e2z);
            System.out.println("spieler3 legt: " + e3f + "_" +e3z);
            System.out.println("spieler3 legt: " + e4f + "_" +e4z);
            spieler3.entferne(e4f, e4z);
            spieler3.entferne(e3f , e3z);
            spieler3.entferne(e2f,e2z );
            spieler3.entferne(e1f, e1z );

            case 3:
            System.out.println("spieler3 legt: " + e1f + "_" +e1z);
            System.out.println("spieler3 legt: " + e2f + "_" +e2z);
            System.out.println("spieler3 legt: " + e3f + "_" +e3z);
            spieler3.entferne(e3f , e3z);
            spieler3.entferne(e2f,e2z );
            spieler3.entferne(e1f, e1z );
            mitte.setzeMehrlinge(3);
            break;

            case 2:
            System.out.println("spieler3 legt: " + e1f + "_" +e1z);
            System.out.println("spieler3 legt: " + e2f + "_" +e2z);

            //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 2) {

            spieler3.entferne(e2f,e2z );
            spieler3.entferne(e1f, e1z );
            mitte.setzeMehrlinge(2);
            //}
            break;

            case 1:
            System.out.println("spieler3 legt: " + e1f + "_" +e1z);
            spieler3.entferne(e1f, e1z );
            mitte.setzeMehrlinge(1);

            //if(spieler3.MehrlingsCheck2(spieler3.Legen3(mitte.gebeMehrlinge(), mitte.gebeZahl()).gebeZahl()) == 1) {

            //}
            break;
        }
    }

    

    

    

   
    public boolean TrumpfCheckS1(){ 
        //überprüft, ob der Spieler einen Trumpf hat 
        if(spieler2.gebeAusgesetzt() == true && spieler3.gebeAusgesetzt() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean TrumpfCheckS2(){
        //überprüft, ob der Spieler einen Trumpf hat 
        if(spieler1.gebeAusgesetzt() == true && spieler3.gebeAusgesetzt() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean TrumpfCheckS3(){
        //überprüft, ob der Spieler einen Trumpf hat 
        if(spieler1.gebeAusgesetzt() == true && spieler2.gebeAusgesetzt() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public void gebeAusgesetztTest() {
        //Test-Methode
        System.out.println("Spieler 1 ausgesetzt: " + spieler1.gebeAusgesetzt());
        System.out.println("Spieler 2 ausgesetzt: " + spieler2.gebeAusgesetzt());
        System.out.println("Spieler 3 ausgesetzt: " + spieler3.gebeAusgesetzt());
    }

    public Spieler gewonnen() {
        if(spieler1.kleinstesBlatt == null) {
            return spieler1;
        }

        if(spieler2.kleinstesBlatt == null) {
            return spieler2;
        }

        if(spieler3.kleinstesBlatt == null) {
            return spieler3;
        }
        else{ 
            return null;
        }

    }

    public Mitte gebeMitte() {
        return mitte;
    }

    /*  public void gebeKartenAnzahl(Spieler spieler) {
    spieler.gebeKartenAnzahl();
    }
     */    
    public void MitteÄndern(int mehrlinge, int zahl) {
        mitte.setzeMitte(mehrlinge, zahl);
    }

    public void GeneralHandprint() { //Gibt alle Hände der Spieler als Outprint raus 
        spieler1.HandPrint();
        System.out.println("");
        spieler2.HandPrint();
        System.out.println("");
        spieler3.HandPrint();
    }

    //methode zur Auswahl eines zufälligen Felds der Spielkarten-Arrays, und Rückgabe der Spielkarte
    //public Spielkarte RandomConvert2Spielkarte() {
    //  int x = (int) (Math.random()) * 36;

    //return spielkarte[x];
    //}
    //public Spielkarte kErstellenReturnen() {
    //   int x = (int) (Math.random()) * 36;
    //   Spielkarte k = spielkarte[x];
    //   return k;
    //}
    //public void DeckErstellen2 () {
    //  for(int i = 0; i <= 35 ; i++) {
    //     for(int z = 0; z <= 8; z++) {
    //      for(int f = 0; f <= 3; f++) {
    //         spielkarte [i] = new Spielkarte(z, f);
    //    System.out.println("Spielkarte Nummer" + i + "mit Zahl" + z + "und Farbe" + f + "wurde erstellt");
    //     
    //}
    // }
    // }
    //}

    public void SpielkarteHinzufuegen() {
    }

    /*public void Austeilen2 () {
    for(i = 0; i>0; i = i - 1) {
    if (i > 0) {

    zahl =(int) Math.random()*4 ;
    farbe = (int) Math.random()*9;

    }
    else{
    }

    }
    } */
    //Math.random()
    public void randomOutPrint () {
        farbe = (int) (Math.random()*4);
        zahl = (int) (Math.random()*9) ;
        //System.out.println ("Farbe " + farbe);
        //System.out.println ("Zahl " + zahl);
    }

    public void Würfeln (){
        int i = (int) (Math.random()*6)+1;
        System.out.println("Augenzahl" + i);

    }

    public void EntferneS1(int f, int z) {
        spieler1.entferne(f,z); 
    }

    public void EntfTest(int F, int Z){

        spieler1.entferne(F,Z);
        spieler2.entferne(F,Z);
        spieler3.entferne(F,Z);
        System.out.println("Karten der Spieler:");
        System.out.println("");

        spieler1.HandPrint();
        System.out.println("");
        spieler2.HandPrint();
        System.out.println("");
        spieler3.HandPrint();

    }

    public void Entf2Test() {
        spieler1.entferne2(spieler1.gebeKleinstesBlatt());
        System.out.println("Karten der Spieler:");
        System.out.println("");

        spieler1.HandPrint();
        System.out.println("");
        spieler2.HandPrint();
        System.out.println("");
        spieler3.HandPrint();

    }
    public void A_Entf2TestbS2() {
        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                        spieler2.entferne2(spieler1.gebeKleinstesBlatt());
                                                                                spieler2.entferne2(spieler1.gebeKleinstesBlatt());
        System.out.println("Karten der Spieler:");
        System.out.println("");

        spieler1.HandPrint();
        System.out.println("");
        spieler2.HandPrint();
        System.out.println("");
        spieler3.HandPrint();

    }

    

    

    public void AkutelleZahlEntfernenTestS1() {
        spieler1.AkutelleZahlEntfernen();
        GeneralHandprint();
        System.out.println("Mehrlinge bei kleinstesBlatt: " + spieler1.AkutelleZahlEntfernen());
    }

    public void MitteSetzen(int m, int z) {
        mitte.setzeMitte(m, z);
        MitteOutprint();
        GeneralHandprint();
    }

    public void MitteOutprint() {
        System.out.println("Mitte: M: " + mitte.gebeMehrlinge() + " Z: " + mitte.gebeZahl());
    }

    public void MehrlingOutPrint2(int confirm) {
        System.out.println("Spieler 1 Mehrlinge von Zahl " + confirm +": " + spieler1.MehrlingsCheck2(confirm));
        System.out.println("Spieler 2 Mehrlinge von Zahl " + confirm +": " + spieler2.MehrlingsCheck2(confirm));
        System.out.println("Spieler 3 Mehrlinge von Zahl " + confirm +": " + spieler3.MehrlingsCheck2(confirm));
    }

    

    public void HerzSechsCheckTest() {
        //System.out.println ("Spieler 1: " + spieler1.HerzSechsCheck());
        //System.out.println ("Spieler 2: " + spieler2.HerzSechsCheck());
        //System.out.println ("Spieler 3: " + spieler3.HerzSechsCheck());
    }

    

    public Listenelement WipeOutNumber2S1(int w) { 

        GeneralHandprint();
        return spieler1.WipeOutNumber2(w);

    }

    public Listenelement WipeOutNumber2S2(int w) {
        GeneralHandprint();
        return spieler2.WipeOutNumber2(w);

    }

    public Listenelement WipeOutNumber2S3(int w) {
        GeneralHandprint();
        return spieler3.WipeOutNumber2(w);

    }

    public Listenelement WipeOutTestReturn(int w) {
        return spieler1.WipeOutNumber(w);
    }

    

   
    public int MehrlingsCheck2TestBeiII() {
        return spieler1.gebeKleinstesBlatt().gebeNachfolger().MehrlingsCheck2(spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl());
    }

    

    public void entfTest3() {
        spieler1.gebeKleinstesBlatt().gebeNachfolger().entferne(spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeFarbe(), spieler1.gebeKleinstesBlatt().gebeNachfolger().gebeZahl());
        GeneralHandprint();
    }

    public Listenelement Legen4TestS1b(){
        Listenelement s = spieler1.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        GeneralHandprint();
        return s;
    }

    public Listenelement Legen4TestS2b(){
        Listenelement s = spieler2.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        GeneralHandprint();
        return s;
    }

    public Listenelement Legen4TestS3b(){
        Listenelement s = spieler3.Legen4(mitte.gebeMehrlinge(), mitte.gebeZahl());
        GeneralHandprint();
        return s;

    }

    public void gebeAnzahlKarten() {
        System.out.println("spieler1 Kartenanzahl: " + spieler1.gebeAnzahlKarten());
        System.out.println("spieler2 Kartenanzahl: " + spieler2.gebeAnzahlKarten());
        System.out.println("spieler3 Kartenanzahl: " + spieler3.gebeAnzahlKarten());
    }

    //  
}

